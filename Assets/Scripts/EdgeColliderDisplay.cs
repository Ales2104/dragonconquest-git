﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EdgeColliderDisplay : MonoBehaviour
{
    EdgeCollider2D _edge;
    public Color color = Color.magenta;

    void Awake()
    {
        _edge = GetComponent<EdgeCollider2D>();    
    }

    void OnDrawGizmos()
    {
        Gizmos.color = color;
        for (int i = 1; i < _edge.pointCount; i++)
        {
            Gizmos.DrawLine(_edge.points[i - 1], _edge.points[i]);
        }
    }
}
