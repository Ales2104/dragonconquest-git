﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //privates
    Human _human;
    SpriteRenderer _humanSpriteRender;
    bool _isDeadZoneOnRight;
    bool _isDeadZoneOnBottom;
    float _rightViewportPointAux;
    float _leftViewportPointAux;
    float _topViewportPointAux;
    float _bottomViewportPointAux;
    //Movimientos con respecto a la camara, si el deadzone tiene que ir hacia la derehca, la camara se mueve hacia la izq(_hasToMoveToLeft)
    bool _hasToMoveToRight;
    bool _hasToMoveToLeft;
    bool _hasToMoveToBottom;
    bool _hasToMoveToTop;

    //publics
    [Header("How fast camera moves when deadzone changes.")]
    [Range(0f, 5f)]
    public float smooth = 1;

    [Header("Distancias entre el centro de la camara y el PJ")]
    [Range(-10f, 10f)]
    public float horizontalPosFromPlayer = 5f;
    [Range(-10f, 10f)]
    public float verticalPosFromPlayer = 2f;


    [Header("Posiciones PJ para mover cámara con respecto a ésta. ViewPort space(0,1)")]
    [Tooltip("Activar mov cuando el jugador esté a tantas unidades del lado derecho")]
    [Range(0f, 1f)]
    public float rightHumanPosToMove = 0.6f;

    [Tooltip("Activar mov cuando el jugador esté a tantas unidades del lado izq")]
    [Range(0f, 1f)]
    public float leftHumanPosToMove = 0.2f;

    [Tooltip("Activar mov cuando el jugador esté a tantas unidades hacia abajo(respecto al centro)")]
    [Range(0f, 1f)]
    public float downHumanPosToMove = 0.25f;

    [Tooltip("Activar mov cuando el jugador esté a tantas unidades hacia arriba")]
    [Range(0f, 1f)]
    public float upHumanPosToMove = 0.7f;

    [Space(10f)]
    public bool disableGizmos;

    void Awake()
    {
        _human = GameObject.FindObjectOfType<Human>();
        if (_human == null)
            Debug.LogError("Player is not in scene, or Human Component is not attached");

        _humanSpriteRender = _human.GetComponentInChildren<SpriteRenderer>();

        if (smooth == 0)
            Debug.LogError("smooth speed is 0!");

        SetCameraPositionAtStart();//Antes de comenzar a calcular la posicion del pj en movimiento. Es la posicion default.

        _isDeadZoneOnRight = false ;
        _isDeadZoneOnBottom = true;//siempre empieza En la parte inferior de la camara.

        _rightViewportPointAux = rightHumanPosToMove;
        _leftViewportPointAux = leftHumanPosToMove;
        _bottomViewportPointAux = downHumanPosToMove;
        _topViewportPointAux = upHumanPosToMove;
    }

    void FixedUpdate()
    {
        SetCameraMovement();
    }

    void SetCameraPositionAtStart()
    {
        var humanXPos = _human.transform.position.x;
        var humanYPos = _human.transform.position.y;

        transform.position = new Vector3(humanXPos + horizontalPosFromPlayer, humanYPos + verticalPosFromPlayer, transform.position.z);

    }

    void SetCameraMovement()
    {

        var currentHumanPos = _human.currentPos = _human.transform.position;

        //var humanXPos = _human.transform.position.x;
        var humanXPos = _human.transform.position.x;
        var humanYBottomPos = _humanSpriteRender.bounds.min.y;//pies
        var humanYTopPos = _humanSpriteRender.bounds.max.y;//cabeza

        float rightBoundary = Camera.main.ViewportToWorldPoint(new Vector3(rightHumanPosToMove, 0)).x;
        float leftBoundary = Camera.main.ViewportToWorldPoint(new Vector3(leftHumanPosToMove, 0)).x;
        float topBoundary = Camera.main.ViewportToWorldPoint(new Vector3(0, upHumanPosToMove, 0)).y;
        float bottomBoundary = Camera.main.ViewportToWorldPoint(new Vector3(0, downHumanPosToMove, 0)).y;

        

        var cameraVelocity = (currentHumanPos - _human.lastPos) / Time.deltaTime;


        #region No se usa

        //---------------------ESTO ANDA, LA CAMARA SIGUE AL PERSONAJE A LA VELOCIDAD DE ESTE--------------------------------
        //if (humanXPos >= rightBoundary)
        //    transform.position += new Vector3(cameraVelocity.x * Time.deltaTime , 0 );
        //else if (humanXPos <= leftBoundary)
        //    transform.position -= new Vector3(-cameraVelocity.x * Time.deltaTime, 0);

        //if (humanYPos >= transform.position.y)
        //{
        //    transform.position += new Vector3(0, cameraVelocity.y * Time.deltaTime);
        //}
        //else if (humanYPos < transform.position.y + downHumanPosToMove)
        //{
        //    transform.position -= new Vector3(0, -cameraVelocity.y * Time.deltaTime);
        //}
        //-------------------------------------------------------------------------------------------------------------------

        #endregion


        if (!_isDeadZoneOnRight)
        {
            if (humanXPos >= rightBoundary)
                transform.position += new Vector3(cameraVelocity.x * Time.deltaTime, 0);

            if (humanXPos <= leftBoundary)
            {
                _hasToMoveToLeft = true;
            }

        }
        else
        {
            if (humanXPos <= leftBoundary)
                transform.position -= new Vector3(-cameraVelocity.x * Time.deltaTime, 0);

            if (humanXPos >= rightBoundary)
            {
                _hasToMoveToRight = true;
            }
        }


        if (_isDeadZoneOnBottom)
        {
            if (humanYTopPos >= topBoundary)
                transform.position += new Vector3(0, cameraVelocity.y * Time.deltaTime);

            if ((humanYBottomPos + 0.5f) < bottomBoundary)
            {
                _hasToMoveToBottom = true;
                
            }
        }
        else
        {
            if (humanYBottomPos <= bottomBoundary)
                transform.position += new Vector3(0, cameraVelocity.y * Time.deltaTime);

            if (humanYTopPos >= topBoundary)
            {
                _hasToMoveToTop = true;
                
            }
        }

        /*ESTOS IFS LOS HAGO PARA QUE CUANDO EL PJ TOQUE EL BOUNDARY QUE CAMBIA EL DEADZONE, LA CAMARA SE MUEVA SIN PARAR 
         HASTA QUE LLEGUE AL OTRO LADO.*/
        if (_hasToMoveToRight)
        {
            leftHumanPosToMove = Mathf.Lerp(leftHumanPosToMove, _leftViewportPointAux, smooth * Time.deltaTime);
            rightHumanPosToMove = Mathf.Lerp(rightHumanPosToMove, _rightViewportPointAux, smooth * Time.deltaTime);

            //esta variable se usa en estas dos lineas nomas, por eso le mande ese nombre choto.
            var v = (_human.transform.position.x - rightBoundary) / Time.deltaTime;
            transform.position += new Vector3(v * Time.deltaTime, 0);

            if (leftHumanPosToMove <= (_leftViewportPointAux + 0.05f))
            {
                _isDeadZoneOnRight = false;
                _hasToMoveToRight = false;
                //Debug.Log("los boundaries llegaron al lado izquierdo");
            }
        }
        if (_hasToMoveToLeft)
        {
            //esto mueve los boundaries.
            leftHumanPosToMove = Mathf.Lerp(leftHumanPosToMove, 1 - _rightViewportPointAux, smooth * Time.deltaTime);
            rightHumanPosToMove = Mathf.Lerp(rightHumanPosToMove, 1 - _leftViewportPointAux, smooth * Time.deltaTime);


            //esto a la camara.
            //esta variable se usa en estas dos lineas nomas, por eso le mande ese nombre choto.
            var v = (_human.transform.position.x - leftBoundary) / Time.deltaTime;
            transform.position += new Vector3(v * Time.deltaTime, 0);


            if (rightHumanPosToMove >= 1 - (_leftViewportPointAux + 0.05f))
            {
                _isDeadZoneOnRight = true;
                _hasToMoveToLeft = false;
                //Debug.Log("los boundaries llegaron al lado derecho");
            }
        }
        if (_hasToMoveToBottom)
        {
            downHumanPosToMove = Mathf.Lerp(downHumanPosToMove, 1 - _topViewportPointAux, smooth * Time.deltaTime);
            upHumanPosToMove = Mathf.Lerp(upHumanPosToMove, 1 - _bottomViewportPointAux, smooth * Time.deltaTime);

            //esta variable se usa en estas dos lineas nomas, por eso le mande ese nombre choto.
            var v = (humanYBottomPos - bottomBoundary) / Time.deltaTime;
            transform.position += new Vector3(0, v * Time.deltaTime);

            if (downHumanPosToMove >= (1 - _topViewportPointAux) - 0.05f)
            {
                _isDeadZoneOnBottom = false;
                _hasToMoveToBottom = false;
                //Debug.Log("los boundaries llegaron al lado superior");

            }
        }
        if (_hasToMoveToTop)
        {
            downHumanPosToMove = Mathf.Lerp(downHumanPosToMove, _bottomViewportPointAux, smooth * Time.deltaTime);
            upHumanPosToMove = Mathf.Lerp(upHumanPosToMove, _topViewportPointAux, smooth * Time.deltaTime);

            //esta variable se usa en estas dos lineas nomas, por eso le mande ese nombre choto.
            var v = (humanYTopPos - topBoundary) / Time.deltaTime;
            transform.position += new Vector3(0, v * Time.deltaTime);

            if (downHumanPosToMove <= (_bottomViewportPointAux + 0.05f))
            {
                _isDeadZoneOnBottom = true;
                _hasToMoveToTop = false;
                //Debug.Log("los boundaries llegaron al lado inferior");

            }
        }

        _human.lastPos = _human.transform.position;
    }

    

    void OnDrawGizmos()
    {
        if (!disableGizmos)
        {
            //Gizmos.DrawSphere(_humanSpriteRender.bounds.max, 0.2f);

            Gizmos.color = Color.red;


            Gizmos.DrawLine(Camera.main.ViewportToWorldPoint(new Vector3(leftHumanPosToMove, 0.3f, 5)),
                            Camera.main.ViewportToWorldPoint(new Vector3(leftHumanPosToMove, 0.6f, 5)));

            Gizmos.color = Color.black;

            Gizmos.DrawLine(Camera.main.ViewportToWorldPoint(new Vector3(rightHumanPosToMove, 0.3f, 5)),
                            Camera.main.ViewportToWorldPoint(new Vector3(rightHumanPosToMove, 0.6f, 5)));

            Gizmos.color = Color.blue;

            Gizmos.DrawLine(Camera.main.ViewportToWorldPoint(new Vector3(rightHumanPosToMove, upHumanPosToMove, 5)),
                            Camera.main.ViewportToWorldPoint(new Vector3(leftHumanPosToMove, upHumanPosToMove, 5)));

            Gizmos.color = Color.green;
            Gizmos.DrawLine(Camera.main.ViewportToWorldPoint(new Vector3(rightHumanPosToMove, downHumanPosToMove, 5)),
                            Camera.main.ViewportToWorldPoint(new Vector3(leftHumanPosToMove, downHumanPosToMove, 5)));
        }
        
    }

}
