﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SI ESTA CLASE LA TIENE EL PLAYER ES PORQUE NOMAS MUESTRA EL DAÑO ARRIBA DE ESTE
/// </summary>
public class DummyBehaviour : MonoBehaviour {

    Agent _agent;
    public TextMesh text;
    
    
	// Use this for initialization
	void Start ()
    {
        _agent = GetComponent<Agent>();

        if (text == null)
        {
            Debug.LogError("falta asignar el text mesh");
        }

        if (_agent is Human)
        {

        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        text.text = _agent.health.ToString("0.0");

        if (_agent.health <= 0)
        {
            //_agent.health = 100;

            if (_agent is Enemy)
            {
                _agent.isDead = true;
            }
        }

        if (_agent is Enemy && _agent.isDead)
        {
            _agent.GetComponent<Renderer>().material.color = Color.Lerp(_agent.GetComponent<Renderer>().material.color ,
                                                                        new Color(0,0,0,0),
                                                                        Time.deltaTime *3);
            /*_agent.GetComponentInChildren<Weapon>().GetComponent<Renderer>().material.color = Color.Lerp(_agent.GetComponentInChildren<Weapon>()
                .GetComponent<Renderer>().material.color,
                                                                        new Color(0, 0, 0, 0),
                                                                        Time.deltaTime *3);*/
                
            text.text = "";
        }


    }
}
