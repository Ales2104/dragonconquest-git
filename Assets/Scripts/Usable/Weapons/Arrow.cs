﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour 
{
    Rigidbody2D _rigidBody;
    bool _crashedWall;

    int _wallLayer;
    int _floorLayer;
    int _playerLayer;

	void Start () {
        _rigidBody = GetComponent<Rigidbody2D>();

        _wallLayer = LayerMask.NameToLayer("wall");
        _floorLayer = LayerMask.NameToLayer("Floor");
        _playerLayer= LayerMask.NameToLayer("Player");
	}
	
	
	void Update () 
    {
        if (!_crashedWall)
            transform.rotation = Quaternion.LookRotation(_rigidBody.velocity);   
        
	}

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.layer == _floorLayer || col.gameObject.layer == _wallLayer )
        {
            _crashedWall = true;
            Destroy(_rigidBody);
            Destroy(gameObject, 2.5f);
            
        }
        else if (col.gameObject.layer == _playerLayer)
        {
            _crashedWall = true;
            Destroy(_rigidBody);
            transform.SetParent(col.gameObject.transform.parent);
            Destroy(gameObject, 2.5f);
        }
    }


    
}
