﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : Weapon
{
    public Arrow arrow;
    public Transform spawnPoint;
    public float shootAngle;
    
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void ThrowArrow(List<Vector2> targetPositions , int dir)
    {
        foreach (var target in targetPositions)
        {
			Launch(target , dir);
        }
    }

    void Launch(Vector2 target , int dir)
    {
        var newArrow = Instantiate(arrow, spawnPoint.position, Quaternion.LookRotation(arrow.transform.right));

		//float dist = Vector2.Distance(target, arrow.transform.position);
		var dist = (new Vector3(target.x,target.y,0) - spawnPoint.position ).magnitude ;


        float Vi = Mathf.Sqrt(dist * -Physics.gravity.y / (Mathf.Sin(Mathf.Deg2Rad * shootAngle * 2)));
        float Vx, Vy;

        Vy = Vi * Mathf.Sin(Mathf.Deg2Rad * shootAngle);
        Vx = Vi * Mathf.Cos(Mathf.Deg2Rad * shootAngle) * dir;

        var localVelocity = new Vector2(Vx, Vy);

        newArrow.GetComponent<Rigidbody2D>().velocity = localVelocity;

    }
}
