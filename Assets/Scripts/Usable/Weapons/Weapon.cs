﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : CombatUsable
{


    public event Action OnAttackSucced = delegate { }; //avisa a sus observadores que el ataque se realizo con exito
    

    public string weaponName;

    [Range(0f, 100f)]
    public float baseDamage = 10f;

    public WeaponElement currentElement;

    //este objeto antes era de tipo collider2D. Si se rompe algo del parry es por esto.
    public AttkColliderZone collisionZone;
    Collider2D _collisionZoneCol;//el collider que tiene la prop collisionZone;

    bool _makeExtraDmgOnComboSucced;
    float _extraDmgPercent;//esto es el porcentaje extra que genera el ultimo golpe del combo realizado con exito.
    
    Agent _WhosWearingMe;
    List<Agent> _theOnesInColZone = new List<Agent>();//solo lo uso para chequear que no se le haga daño al mismo agente 2veces

	// Use this for initialization
	void Start ()
    {
        _WhosWearingMe = GetComponentInParent<Agent>();
        _WhosWearingMe.animmatorManager.OnEnableColZone += ActivateCollisionZone;
        _WhosWearingMe.animmatorManager.OnDisableColZone += DisableCollisionZone;

        if (collisionZone == null)
        {
            Debug.LogError("2d collider 'colission zone' in weapon script is null. agent name => '" + _WhosWearingMe.name);
        }
        else
        {
            _collisionZoneCol = collisionZone.GetComponent<BoxCollider2D>();

            _collisionZoneCol.enabled = false;
            collisionZone.OnColliderDetection += GetAgentInColZone;
        }
    }
	
    
    void ActivateCollisionZone()
    {
        _collisionZoneCol.enabled = true;
    }
    public void DisableCollisionZone()
    {
       
        _collisionZoneCol.enabled = false;
        //cuando se termino el golpe(o sea cuando desactivo el collider) pongo la lista en de los que toco el attk zone en 0)
        _theOnesInColZone = new List<Agent>();
    }

    public void StopCurrentAnimation()
    {
        Debug.Log("ojo con esto, es para que no tire error en enemy.");
    }

    public bool IsCollisionZoneEnabled()
    {
        return _collisionZoneCol.enabled;
    }

    /// <summary>
    /// Daña al agente qeu se le pasa por param.
    /// </summary>
    void MakeCommonDamage(float dmg , Agent enemy)
    {
        var partialDmg = (dmg + currentElement.damage) * currentLevel;

        if (_makeExtraDmgOnComboSucced)
        {
            var extradmg = (_extraDmgPercent * partialDmg) / 100;
            partialDmg += extradmg;
            _makeExtraDmgOnComboSucced = false;
        }
        
        enemy.currentArmor.ProcessDamage(partialDmg);
    }

    public void SetExtraDmg(float extra)
    {
        _makeExtraDmgOnComboSucced =  true;
        _extraDmgPercent = extra;
    }
    
    
    /// <summary>
    /// Obtiene al agente(q se le pasa por param) contra el que choco el ColliderZone.
    /// </summary>
    void GetAgentInColZone(Agent pAgent)
    {
        if (_theOnesInColZone.Count == 0 || !_theOnesInColZone.Contains(pAgent))
        {
            _theOnesInColZone.Add(pAgent);
            MakeCommonDamage(baseDamage, pAgent);
            
            if (_WhosWearingMe is Human)
                OnAttackSucced();
        }
    }
    
    

}
