﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickable : IUsable
{
    void OnPickUp(Human h);//se usa al instante(apenas se agarra).
}
