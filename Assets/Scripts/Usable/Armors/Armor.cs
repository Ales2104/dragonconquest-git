﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : CombatUsable
{
    [Range(0f, 100f)]
    public float baseDefence = 10;

    public ArmorElement currentElement;
    public Action OnAgentIsDead = delegate { };
    public Action OnHurtAgent = delegate { };//por ahora para el jugador => activa el bool imbeinghurt para que no se mueva.

    Agent _agent;
    bool _iAmFromhuman;


    public void ProcessDamage(float partialDmg)
    {
        var totalDmg = ((baseDefence + currentElement.defence) * currentLevel) - partialDmg;
        if (totalDmg < 0)
        {
            _agent.health += totalDmg;
            OnHurtAgent();
            if (_iAmFromhuman)
            {
                var h = _agent as Human;
                h.animmatorManager.SetParameter("takeDmg", true);
                
                if (_agent.health <= 0)
                {
                    OnAgentIsDead();
                    h.animmatorManager.SetParameter("isDead", true);
                }
            }
            else
            {
                if (_agent.health < 0)
                {
                    OnAgentIsDead();
                }
            }

            


        }
        
        
    }

	// Use this for initialization
	void Awake()
    {
        _agent = GetComponentInParent<Agent>();
        _iAmFromhuman = _agent is Human;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
