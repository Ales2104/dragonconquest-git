﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotion : MonoBehaviour , IPickable
{
    public float hp = 20;
            
    public void OnPickUp(Human h)
    {
        h.SetHP(hp);
        Destroy(gameObject);
    }
    
}
