﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorManager : MonoBehaviour
{
    #region Publics
    
    
    public Animator animator;
    public Action OnStopTakeDmg = delegate { };
    public event Action OnComboEnabled = delegate { };
    public event Action OnComboDisabled = delegate { };
    public event Action OnAttkEnded = delegate { };//cada vez qeu termina la anim de algun attk, esta funcion es llamada
    public event Action OnEnableColZone = delegate { };//activa la zona de colision de atak en algun punto de la anim
    public event Action OnDisableColZone = delegate { };//desactiva la zona de colision de atak en algun punto de la anim

    #endregion

    #region Privates
    bool _isRunning;
    int _numOfAttacks = 0;//chequeo la cantidad para pasar de una anim a otra;
    #endregion

    
    void Start ()
    {
        if (animator == null)   animator = GetComponent<Animator>();
        
        SetParamsToDefault();
    }

    private void Update()
    {
        
    }
    
    public void SetParameter(string name , bool value)
    {
        animator.SetBool(name, value);
    }
    public void SetParameter(string name, float value)
    {
        animator.SetFloat(name, value);
    }
    public void SetParameter(string name, int value)
    {
        animator.SetInteger(name, value);
    }

    /*public void GoToIdleState()
    {
        if (!animator.GetBool("isJumping") &&  !animator.GetBool("Parrying") && !animator.GetBool("takeDmg"))
        {
            //SetParamsToDefault();
        }
    }*/

    public void SetRun(bool v)
    {
        //siempre que se esté moviendo va a configurar el isRunning.
        if (animator.GetFloat("Speed") > 0.01f)
        {
            animator.SetBool("isRunning", v);
        }
        
    }

    public void SetParry()
    {
        animator.SetBool("Parrying", true);
    }

    public void ChangeNumOfAttks(int comboLength)
    {
        if (_numOfAttacks < comboLength)
        {
            _numOfAttacks++;
            SetParameter("numAttks", _numOfAttacks);
        }
        else
        {
            FinishAttkStateMachine();
        }
        
    }
    /// <summary>
    /// setea a 0 el numero de ataques
    /// </summary>
    public void FinishAttkStateMachine()
    {
        _numOfAttacks = 0;
        SetParameter("numAttks", _numOfAttacks);
        //SetParameter("isAttacking", false);
        SetParameter("isLightAttk", false);
    }

    void SetParamsToDefault()
    {
        foreach (var param in animator.parameters)
        {
            switch (param.type)
            {
                case AnimatorControllerParameterType.Float:
                    animator.SetFloat(param.name, param.defaultFloat);
                    break;
                case AnimatorControllerParameterType.Int:
                    animator.SetInteger(param.name, param.defaultInt);
                    break;
                case AnimatorControllerParameterType.Bool:
                    animator.SetBool(param.name, param.defaultBool);
                    break;
                case AnimatorControllerParameterType.Trigger:
                    animator.SetTrigger(param.name);
                    break;
            }
        }
    }

    /// <summary>
    /// Animation Event, se ejecuta cuando se termina la anim
    /// </summary>
    void StopParry()
    {
        animator.SetBool("Parrying",false);

    }

    void StopTakeDmg()
    {
        OnStopTakeDmg();
        animator.SetBool("takeDmg", false);
    }

    void StopDeadAnim()
    {
        animator.SetBool("DeadAnimFinish", true);
    }

    /// <summary>
    /// Pausa la anim actual o la despausa, segun el parametro
    /// </summary>
    public void FreezeCurrentAnimation(bool pause)
    {
        if (pause)
            animator.speed = 0;
        else
            animator.speed = 1;
    }

    void SetComboToEnabled()
    {
        OnComboEnabled();
    }
    void SetComboToDisabled()
    {
        OnComboDisabled();
    }
    /// <summary>
    /// Se ejecuta al final de cada animacion de ataque.
    /// </summary>
    void AttackAnimHasEnded()
    {
        OnAttkEnded();
    }

    void ActivateCollisionZone()
    {
        OnEnableColZone();
    }
    public void DisableCollisionZone()
    {
        OnDisableColZone();
    }

    /// <summary>
    /// Animation Event, se ejecuta cuando el pj deja de dashear
    /// </summary>
    void StopDash()
    {
        animator.SetBool("isDashing", false);
    }
    /// <summary>
    /// Animation Event, se ejecuta cuando el pj deja de subir
    /// </summary>
    void JumpUp()
    {

    }
    /// <summary>
    /// Animation Event, se ejecuta cuando el pj empieza a bajar
    /// </summary>
    void JumpDown()
    {

    }

    /// <summary>
    /// Animation Event, se ejecuta cada vez que la anim pisa el suelo.(PorEjemplo: para reproducir sonido)
    /// </summary>
    void WalkStep()
    {

    }
}
