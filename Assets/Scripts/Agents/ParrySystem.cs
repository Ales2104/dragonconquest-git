﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ParrySystem : MonoBehaviour
{

    Agent _currentAgent;
    bool _iAmHuman;
    MeshRenderer _feedBack;
    Collider2D _collider;
    List<Agent> _theParriedOnes = new List<Agent>();//los que van a ser "parreados".
    List<Agent> _theOnesInsideTrigger = new List<Agent>();//lo que se encuentran dentro del trigger;
    float _stunTime;

    public bool isInParryMode;
    [Header("Collider offset when parry backwards")]
    [Range(-4f, 4f)]
    public float backColliderOffset = -3;//es la posicion(offset) del collider cuando el agent hace parry para el otro lado. 
    float forwardColliderOffset;//es el offset original en donde se encuentra el collider
    public Action OnParryStop = delegate { };

	void Awake()
    {

        _feedBack = GetComponentInChildren<MeshRenderer>();//solo para el humano. es de prueba.
        _collider = GetComponent<Collider2D>();
        forwardColliderOffset = _collider.offset.x;

        ShowFeedback(false);

        _currentAgent = GetComponentInParent<Agent>();
        
        _iAmHuman = _currentAgent is Human; //si es humano(jugador) true, si no, es enemigo, entonces false.

        _stunTime = _currentAgent.meleeStuntTime;
    }
	

    public void StartParry(int agentDirection , float horizAxis )
    {
        if (isInParryMode) return;

        if (_iAmHuman)
        {
            var human = _currentAgent as Human;
            human.animmatorManager.SetParry();
        }

        SetParryColliderPositionByDir(agentDirection, horizAxis);
        ShowFeedback(true);
        isInParryMode = true;
        StartCoroutine(TimeToFinishParry());
    }

    IEnumerator TimeToFinishParry()
    {
        yield return new WaitForSeconds(_currentAgent.parryTime);
        isInParryMode = false;
        OnParryStop();
        ShowFeedback(false);
        _theOnesInsideTrigger = new List<Agent>();
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (isInParryMode)
        {
            var _agent = col.GetComponent<Agent>();

            if (_agent is Enemy && !_iAmHuman || _agent == null) return;//si contra el que colisiono es un enemigo y el currentAgent(quien está parreando) es enemigo, retorno.
            

            if (_theOnesInsideTrigger.Count == 0 || !_theOnesInsideTrigger.Any(a => a == _agent))
            {
                _theOnesInsideTrigger.Add(_agent);

                if (_agent.currentWeapon.IsCollisionZoneEnabled())
                    _agent.GetParry(_stunTime);

            }

        }
    }

    void ShowFeedback(bool activate)
    {
        _feedBack.enabled = activate;
    }
    

    /// <summary>
    /// setea la verdadera posicion del collider tomando como parametro la direccion del agente(por si parrea hacia atras)
    /// </summary>
    void SetParryColliderPositionByDir(int dir , float horizAxis)
    {
        if (dir == 1 && horizAxis == -1)
        {
            _collider.offset = new Vector2(backColliderOffset, _collider.offset.y);

        }
        else if (dir == 1 && horizAxis == 1)
        {
            _collider.offset = new Vector2(forwardColliderOffset, _collider.offset.y);
        }
        else if (dir == -1 && horizAxis == 1)
        {
            _collider.offset = new Vector2(backColliderOffset, _collider.offset.y);
        }
        else if (dir == -1 && horizAxis == -1)
        {
            _collider.offset = new Vector2(forwardColliderOffset, _collider.offset.y);
        }



        SetFeedbackPos(dir, horizAxis);
    }


    /// <summary>
    /// SOLO ESTA HECHO PARA EL JUGADOR.(esta hardcodeado)
    /// </summary>
    void SetFeedbackPos(int direc, float horizontalAxs)
    {
        if (!_iAmHuman) return;
        

        if (direc == 1 && horizontalAxs == 1)
        {
            _feedBack.transform.localPosition = new Vector3(1, 0, transform.localPosition.z);
        }
        else if (direc == 1 && horizontalAxs == -1)
        {
            _feedBack.transform.localPosition = new Vector3(-1, 0, transform.localPosition.z);
        }
        else if (direc == -1 && horizontalAxs == -1)
        {
            _feedBack.transform.localPosition = new Vector3(1, 0, transform.localPosition.z);
        }
        else if (direc == -1 && horizontalAxs == 1)
        {
            _feedBack.transform.localPosition = new Vector3(-1, 0, transform.localPosition.z);
        }
    }
}
