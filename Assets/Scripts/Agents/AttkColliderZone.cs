﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SOLO SETEA EL ONTRIGGERSTAY Y MEDIANTE DELEGATE AVISA AL CURRENT AGENT QUE CHOCO CON UN ENEMIGO. PARA QUE EL AGENT SE ENCARGUE DE 
/// SACARLE VIDA.
/// </summary>
public class AttkColliderZone : MonoBehaviour
{

    Agent _currentAgent;
    bool _iAmHuman;

    public event Action<Agent> OnColliderDetection = delegate { };//el observador de este evento es el Weapon

    void Start () {
        _currentAgent = GetComponentInParent<Agent>();
        _iAmHuman = _currentAgent is Human;

    }
    

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "TriggerPJ"|| collision.gameObject.tag == "TriggerEnemy")
        {
            
            if (collision.gameObject.GetComponentInParent<Agent>().imUntouchable) return;
            
            var h = collision.gameObject.GetComponentInParent<Human>();
            var e = collision.gameObject.GetComponentInParent<Enemy>();

            if (!_iAmHuman)
            {
                if (h != null)
                    OnColliderDetection(h);
            }
            else
            {
                
                if (e != null)
                    OnColliderDetection(e);
            }
        }
    }


}
