﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bowman : Enemy
{
    [Header("Bowman")]
    [Space(10)]
    [Range(0f,10f)]
    public float distanceToGetOutRange = 3f;
    public Color distanceToGetOutRangeColor = Color.blue;
    [Tooltip("Delay before get out of range.")]
    public float timeToGetOutRange = 2f;//tiempo de delay antes de correr hacia el otro lado.

    [Tooltip("When fire more than one arrow")]
    public float distanceBetweenArrows = 2.5f;

    
    GetOutRangeState _getOutRangeState;
    protected override void Awake()
    {
        base.Awake();
        _getOutRangeState = new GetOutRangeState(this,zoneOfAct, rigidBody);
        _getOutRangeState.Transitions = new Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>>();
        _getOutRangeState.Transitions.Add(Inputs.CommonInputs.NothingHappends,idleState);
        //_getOutRangeState.Transitions.Add(Inputs.CommonInputs.PlayerOnRange, combatModeState);

        combatModeState.Transitions.Add(Inputs.CommonInputs.PlayerTooClose, _getOutRangeState);
        stunnedState.Transitions.Add(Inputs.CommonInputs.PlayerTooClose, _getOutRangeState);

    }

    protected override void Update()
    {
        base.Update();
        CheckSensors();
    }

    void CheckSensors()
    {
        IsPlayerToClose();
        GetPlayerOnRange();
        
    }

    void IsPlayerToClose()
    {
        if (fsm.Current != combatModeState && fsm.Current != stunnedState) return;

        if (Vector2.Distance(_human.transform.position, transform.position) <= distanceToGetOutRange)
        {
            fsm.ProcessInput(Inputs.CommonInputs.PlayerTooClose);
        }
    }

    public void StopGetOutRange()
    {
        fsm.ProcessInput(Inputs.CommonInputs.NothingHappends);
    }


    protected override void GetPlayerOnRange()
    {
        if (canAttack && !IsStunned() && !_humanIsDead && fsm.Current != combatModeState)
        {
            var humanPos = new Vector2(_human.transform.position.x, _human.transform.position.y);
            var pjBetweenMyBoundaries = humanPos.x < zoneOfAct.rightPos.x && humanPos.x > zoneOfAct.leftPos.x;

            if (pjBetweenMyBoundaries &&
                Vector2.Distance(transform.position, _human.transform.position) >= distanceToGetOutRange)
            {
                Debug.Log("tiene que ir a CombatMode");
                SetXDirFromOtherObject(_human.transform.position);
                fsm.ProcessInput(Inputs.CommonInputs.PlayerOnRange);

            }
            else if (!pjBetweenMyBoundaries)
            {
                if (Vector2.Distance(transform.position, _human.transform.position) <= playerOnRangeDistance &&
                    Vector2.Distance(transform.position, _human.transform.position) >= distanceToGetOutRange)
                {
                    Debug.Log("tiene que ir a CombatMode");
                    SetXDirFromOtherObject(_human.transform.position);
                    fsm.ProcessInput(Inputs.CommonInputs.PlayerOnRange);
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = playerOnRangeDistColor;
        
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x + playerOnRangeDistance, transform.position.y, transform.position.z));
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x - playerOnRangeDistance, transform.position.y, transform.position.z));

        Gizmos.color = distanceToGetOutRangeColor;
        Gizmos.DrawWireSphere(transform.position, distanceToGetOutRange);
    }
}
