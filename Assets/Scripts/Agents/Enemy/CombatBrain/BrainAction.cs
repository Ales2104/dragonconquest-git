﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainAction 
{
    public string name;
    public int countRepetitions;
    public Inputs.CombatThink reaction;
}
