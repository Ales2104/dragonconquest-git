﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brain 
{

    List<BrainAction> _actions = new List<BrainAction>();

    public void AddAction(string name , Inputs.CombatThink react)
    {
        if (!_actions.Any(a => a.name == name))
        {
            var action = new BrainAction() { name = name, countRepetitions = 1 , reaction = react };
            _actions.Add(action);
        }
        else
        {
            var a = _actions.FirstOrDefault(ac => ac.name == name);
            a.countRepetitions++;
        }
    }

    public Inputs.CombatThink ExcecuteThink(int dif)
    {
        if (_actions.Count == 0)
        {
            return Inputs.CombatThink.simpleAttk;
        }

        var resultName = GetNameOfMoreRepeatedAction();

        return FinalDecision(_actions.Where(a => a.name == resultName).First() , dif).reaction;
    
        
        
    }

    /// <summary>
    /// agrega las acciones hechas 
    /// </summary>
    BrainAction FinalDecision(BrainAction ba , int secondsPassed)//segundos que pasaron antes de que se ejecute el think().
    {

        if (ba.countRepetitions > 4)
        {
            var aux = UnityEngine.Random.Range(0f, 1f);

            if (secondsPassed <= 2.5f)
            {
                if (aux <= .2f)
                    return ba;
                else
                {
                    var selected = _actions[UnityEngine.Random.Range(0, _actions.Count)];
                    while (selected.reaction == Inputs.CombatThink.didParry)
                    {
                        selected = _actions[UnityEngine.Random.Range(0, _actions.Count)];
                    }

                    return selected;
                }
            }
            else
            {
                if (aux <= 0.8f)
                {
                    return ba;
                }
                else
                {
                    var selected = _actions[UnityEngine.Random.Range(0, _actions.Count)];
                    while (selected.reaction == Inputs.CombatThink.didParry)
                    {
                        selected = _actions[UnityEngine.Random.Range(0, _actions.Count)];
                    }

                    return selected;
                }
            }
        }
  
        return ba;
    }

    string GetNameOfMoreRepeatedAction()
    {
        return _actions.Aggregate("",
                   (s, a) =>
                   {
                       if (s == "")
                       {
                           return _actions.First().name;
                       }
                       else
                       {
                           return _actions.Where(action => action.name == s)
                           .Where(action => action.countRepetitions > a.countRepetitions)
                           .First().name;
                       }
                   });
    }

    public void EraseAllActions()
    {
        _actions = new List<BrainAction>();
    }

}
                                                                                                   