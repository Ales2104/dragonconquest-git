﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneOfAction : MonoBehaviour
{

    Enemy _enemy;

    public GameObject leftBound;
    public GameObject rightBound;

    [HideInInspector]
    public Vector2 leftPos;
    [HideInInspector]
    public Vector2 rightPos;

    // Use this for initialization
    void Start () {
        _enemy = GetComponentInParent<Enemy>();

        if (leftBound == null )
        {
            Debug.Log("left bound of enemy named: "+ _enemy.name + " is null");
        }
        if (rightBound == null)
        {
            Debug.Log("right bound of enemy named: " + _enemy.name + " is null");
        }

        leftPos = leftBound.transform.position;
        rightPos = rightBound.transform.position;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
