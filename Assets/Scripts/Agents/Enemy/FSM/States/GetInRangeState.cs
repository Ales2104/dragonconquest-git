﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetInRangeState :  IState<Inputs.CommonInputs>
{
    Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> _transitions;
    Human _human;
    Enemy _thisEnemy;
    Rigidbody2D _rb;

    public GetInRangeState(Human h,Enemy e , Rigidbody2D rb)
    {
        _human = h;
        _thisEnemy = e;
        _rb = rb;
    }

    public void Enter()
    {
        var dir = (_human.transform.position - _thisEnemy.transform.position).normalized.x;
        _thisEnemy.transform.right = -Vector3.Normalize(new Vector3(dir, 0, 0));
    }

    public void Update()
    {
        _rb.position = Vector2.Lerp(_rb.position
                                    , new Vector2(_human.transform.position.x
                                                , _rb.position.y)
                                    , Time.deltaTime * (_thisEnemy.walkSpeed / 2));
    }

    public void Exit()
    {

    }

    public Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> Transitions
    {
        get { return _transitions; }
        set { _transitions = value; }
    }
    
}
