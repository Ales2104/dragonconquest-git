﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfHealState :  IState<Inputs.CommonInputs> 
{

    Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> transitions;
    
    Enemy _enemy;
    Color initColor;
    float startHP;
    float _selfHealMultiplierAux;
    bool _startSelfHealing;

    public SelfHealState(Enemy e)
    {
        _enemy = e;
        startHP = e.health;
        _selfHealMultiplierAux = e.selfHealMultiplier;
        initColor = _enemy.GetComponent<Renderer>().material.color;
    }

    public void Enter()
    {
        
        if (_enemy.health >= startHP)
        {
            Debug.Log("heath is ok");
            _enemy.health = startHP;
            _enemy.isSelfHealing = false;
            _startSelfHealing = false;
        }
        else
        {
            
            _enemy.isSelfHealing = true;
            _startSelfHealing = true;
            _enemy.GetComponent<Renderer>().material.color = Color.green;
        }
    }


    public void Update()
    {
        if (_startSelfHealing)
        {
            _selfHealMultiplierAux -= Time.deltaTime;

            if (_selfHealMultiplierAux < 0 && _enemy.health < startHP)
            {
                _selfHealMultiplierAux = _enemy.selfHealMultiplier;

                var realHPToSum = (startHP * _enemy.selfHealPercent / 100);
                _enemy.health += realHPToSum;

                if (_enemy.health >= startHP)
                {
                    Debug.Log("heath is ok");
                    _enemy.health = startHP;
                    _enemy.isSelfHealing = false;
                    _startSelfHealing = false;
                }
            }
        }

    }
    public void Exit()
    {
        _enemy.GetComponent<Renderer>().material.color = initColor;
    }

    public Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> Transitions
    {
        get { return transitions; }
        set { transitions = value; }
    }
}
