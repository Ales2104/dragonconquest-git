﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IState<Inputs.CommonInputs>
{

    Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> transitions;
    
    public IdleState()
    {
        
    }

    public void Enter()
    {

    }

    
    public void Update ()
    {
	}

    public void Exit()
    {

    }

    public Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> Transitions
    {
        get { return transitions; }
        set { transitions = value; }
    }
}
