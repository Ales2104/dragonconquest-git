﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunnedState : IState<Inputs.CommonInputs>
{

    float _timeStunned;
    float _auxTimeStunned;
    Human _human;
    Agent _thisAgent;
    Color _initColor;

    Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> transitions;

    public StunnedState(float pTimeStunned , Human pCurrentAgent , Agent pThis)
    {
        _timeStunned = pTimeStunned;
        _auxTimeStunned = pTimeStunned;

        _human = pCurrentAgent;
        _thisAgent = pThis;
        _initColor = _thisAgent.GetComponent<Renderer>().material.color; 
    }

    
    public void Enter()
    {
        _thisAgent.GetComponent<Renderer>().material.color = Color.green;
    }

    
    public void Update()
    {

    }

    public void Exit()
    {
        _thisAgent.GetComponent<Renderer>().material.color = _initColor;
    }

    public Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> Transitions
    {
        get { return transitions; }
        set { transitions = value; }
    }
}
