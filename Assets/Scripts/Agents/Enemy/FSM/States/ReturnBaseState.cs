﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnBaseState : IState<Inputs.CommonInputs>
{

    Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> transitions;
    
    Vector3 _startPos;
    Enemy _enemy;
    Rigidbody2D _rb;
    float _pauseTime;
    float _pauseTimeAux;
    float dir;

    public ReturnBaseState(Enemy e, Rigidbody2D rb, Vector3 pXStartPos, float pPauseT)
    {
        _enemy = e;
        _rb = rb;
        _startPos = pXStartPos;
        _pauseTime = pPauseT;

    }

    public void Enter()
    {

        _pauseTimeAux = _pauseTime;
        dir = (_startPos - _enemy.transform.position).normalized.x;
    }


    public void Update()
    {
        _pauseTimeAux -= Time.deltaTime;
        if (_pauseTimeAux < 0)
        {

            _enemy.transform.right = -Vector3.Normalize(new Vector3(dir, 0, 0));

            /*_rb.position = Vector2.Lerp(_rb.position
                            , new Vector2(_startPos.x
                                        , _rb.position.y)
                            , Time.deltaTime * (_enemy.walkSpeed / 3));*/
            _rb.position = Vector2.MoveTowards(_rb.position
                            , new Vector2(_startPos.x
                                        , _rb.position.y)
                            , Time.deltaTime * (_enemy.walkSpeed /2));
        }

    }

    public void Exit()
    {

    }

    public Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> Transitions
    {
        get { return transitions; }
        set { transitions = value; }
    }

}
