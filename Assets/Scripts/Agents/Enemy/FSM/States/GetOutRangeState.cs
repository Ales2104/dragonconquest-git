﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetOutRangeState : IState<Inputs.CommonInputs>
{
    Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> _transitions;
    Bowman _enemy;
    Rigidbody2D _rb;
    ZoneOfAction _zoneOfAct;
    Vector2 _toGo;
    float _timeToRun;
    float _timeToRunAux;

    public GetOutRangeState(Bowman e , ZoneOfAction zoa , Rigidbody2D rb)
    {
        _enemy = e;
        _timeToRun = e.timeToGetOutRange;
        _zoneOfAct = zoa;
        _rb = rb;
    }

    public void Enter()
    {
        _timeToRunAux = _timeToRun;
        var distToLeft = Vector2.Distance(_enemy.transform.position , _zoneOfAct.leftPos);
        var distToRight = Vector2.Distance(_enemy.transform.position, _zoneOfAct.rightPos);

        _toGo = distToLeft > distToRight ? _zoneOfAct.leftPos : _zoneOfAct.rightPos;
    }

    public void Update()
    {
        _timeToRunAux -= Time.deltaTime;
        if (_timeToRunAux < 0)
        {
            _rb.position = Vector2.MoveTowards(_rb.position
                                            , new Vector2(_toGo.x
                                                        , _rb.position.y)
                                            , Time.deltaTime * (_enemy.runSpeed));

            var dist = Mathf.Abs(_toGo.x - _rb.position.x);
            if (dist < 0.1)
            {
                _enemy.StopGetOutRange();   
            }
        }
        
    }


    public void Exit()
    {

    }

    public Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> Transitions
    {
        get { return _transitions; }
        set { _transitions = value; }
    }
}
