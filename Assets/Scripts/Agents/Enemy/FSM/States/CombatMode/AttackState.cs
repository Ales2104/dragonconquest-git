﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IState<Inputs.CombatThink>
{

    public Action OnStopAttack = delegate { };

    //Weapon _currentWeapon;
    //float _timeToAttk = 0.6f;
    //float _timeAux;
    //MeshRenderer _dummyPunchText;


    Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>> _transitions;

    public AttackState(Weapon pCurrentWeapon )
    {

        //_currentWeapon = pCurrentWeapon;
        //_timeAux = _timeToAttk;

        //_dummyPunchText = GameObject.FindGameObjectWithTag("DummyPunchText").GetComponent<MeshRenderer>();
    }

    public virtual void Enter()
    {
        /*_timeAux = _timeToAttk;*/
        
    }

    public virtual void Update()
    {
        /*if (_currentWeapon.IsCurrentAnimFinished()) //si no termino la animacion que no reste el tiempo para volver a ejecutarla.
        {

            _timeAux -= Time.deltaTime;
            if (_timeAux > .25f && _timeAux < .6f)
            {
                _dummyPunchText.enabled = true;
            }
            if (_timeAux < 0)
            {
                _dummyPunchText.enabled = false;
                
                _currentWeapon.PlayAnimation("EnemySimpleAttk");

                OnStopAttack();
            }
        }*/
        
    }

    public virtual void Exit()
    {

    }

    public Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>> Transitions
    {
        get { return _transitions; }
        set { _transitions = value; }
    }

}
