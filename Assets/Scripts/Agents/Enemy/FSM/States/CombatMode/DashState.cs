﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashState : IState<Inputs.CombatThink>
{
    public Action OnDashHasEnded = delegate { };

    Enemy _enemy;
    DashSystem _dashSys;
    ZoneOfAction _zoneOfAct;
    int _dashDir ;

    Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>> _transitions;

    public DashState(Enemy e , ZoneOfAction zoa, DashSystem dashsys)
    {
        _dashSys = dashsys;
        
        _zoneOfAct = zoa;
        _enemy = e;
    }

    public void Enter()
    {
        _dashSys.OnStopDash += DashHasEnded;

        var distToTravelOnDash = _dashSys.timeDashing * _dashSys.speed * _enemy.runSpeed;

        

        float distToFrontBoundary;

        if (_enemy.xAxisDirection() *-1 == 1)
        {
            distToFrontBoundary = Mathf.Abs(_zoneOfAct.rightPos.x - _enemy.transform.position.x);

            if (distToTravelOnDash < distToFrontBoundary)
            {
                _dashDir = 1;
                _dashSys.ExecuteDash(_dashDir);
            }
            else
            {
                //si voy para el lado contrario y tengo espacio, entonces dasheo, si no, salgo del estado.
                distToFrontBoundary = Mathf.Abs(_zoneOfAct.leftPos.x - _enemy.transform.position.x);

                if (distToTravelOnDash < distToFrontBoundary)
                {
                    _dashDir = -1;
                    _dashSys.ExecuteDash(_dashDir);
                }
                else
                    DashHasEnded();

            }
        }
        else
        {
            distToFrontBoundary = Mathf.Abs(_zoneOfAct.leftPos.x - _enemy.transform.position.x);

            if (distToTravelOnDash < distToFrontBoundary)
            {
                _dashDir = -1;
                _dashSys.ExecuteDash(_dashDir);
            }
            else
            {
                //si voy para el lado contrario y tengo espacio, entonces dasheo, si no, salgo del estado.
                distToFrontBoundary = Mathf.Abs(_zoneOfAct.rightPos.x - _enemy.transform.position.x);
                if (distToTravelOnDash < distToFrontBoundary)
                {
                    _dashDir = 1;
                    _dashSys.ExecuteDash(_dashDir);
                }
                else
                    DashHasEnded();
            }
        }
        
        
    }

    public void Update()
    {
        

    }

    public void Exit()
    {
        _dashSys.OnStopDash -= DashHasEnded;
    }

    void DashHasEnded()
    {
        OnDashHasEnded();
    }

    public Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>> Transitions
    {
        get { return _transitions; }
        set { _transitions = value; }
    }
}
