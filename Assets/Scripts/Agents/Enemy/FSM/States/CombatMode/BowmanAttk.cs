﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowmanAttk : AttackState
{
    Bowman _enemy;
    Human _human;
    Bow _currentWeapon;
    List<Vector2> _targetPos;
    float _randomArrows;
    float _timeToThrow;
    float _timeToThrowAux;

    public BowmanAttk(Bow pCurrentWeapon, Human player, Bowman enemy)
        : base(pCurrentWeapon)
    {
        _human = player;
        _currentWeapon = pCurrentWeapon;
        _enemy = enemy;
    }

    public override void Enter()
    {
        base.Enter();
        _targetPos = new List<Vector2>();

        _randomArrows = UnityEngine.Random.Range(0, 1);


        SetRandomArrows(_randomArrows);
        
        _currentWeapon.ThrowArrow(_targetPos, -_enemy.xAxisDirection());

        _targetPos = new List<Vector2>();

        _timeToThrowAux = _timeToThrow;
    }

    public override void Update()
    {
        base.Update();

        if (_randomArrows < .65f)
        {
            _timeToThrowAux -= Time.deltaTime;
            if (_timeToThrow < 0)
            {
                _randomArrows = UnityEngine.Random.Range(0, 1);
                SetRandomArrows(_randomArrows);
                _currentWeapon.ThrowArrow(_targetPos, -_enemy.xAxisDirection());
                _targetPos = new List<Vector2>();
                _timeToThrowAux = _timeToThrow;
            }
        }
        else
        {
            OnStopAttack();
        }
    }


    void SetRandomArrows(float random)
    {
        if (random < .5f)
        {
            _targetPos.Add(_human.transform.position);
        }
        else if (random < .9f)
        {
            _targetPos.Add(_human.transform.position);
            _targetPos.Add(_human.transform.position + new Vector3(_enemy.distanceBetweenArrows, 0, 0));
        }
        else
        {
            _targetPos.Add(_human.transform.position);
            _targetPos.Add(_human.transform.position + new Vector3(_enemy.distanceBetweenArrows, 0, 0));
            _targetPos.Add(_human.transform.position - new Vector3(_enemy.distanceBetweenArrows, 0, 0));
        }
    }

    public override void Exit()
    {
        base.Exit();
        _targetPos = new List<Vector2>();
    }
}
