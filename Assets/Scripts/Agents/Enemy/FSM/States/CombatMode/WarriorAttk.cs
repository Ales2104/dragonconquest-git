﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorAttk : AttackState
{
    Weapon _currentWeapon;
    float _timeToAttk = 0.6f;
    float _timeAux;
    MeshRenderer _dummyPunchText;
    public float asd;
    public WarriorAttk(Weapon pCurrentWeapon) : base(pCurrentWeapon)
    {
        _currentWeapon = pCurrentWeapon;
        _timeAux = _timeToAttk;

        _dummyPunchText = GameObject.FindGameObjectWithTag("DummyPunchText").GetComponent<MeshRenderer>();
    }


    public override void Enter()
    {
        base.Enter();
        _timeAux = _timeToAttk;
    }

    public override void Update()
    {
        base.Update();

        /*if (_currentWeapon.IsCurrentAnimFinished()) //si no termino la animacion que no reste el tiempo para volver a ejecutarla.
        {

            _timeAux -= Time.deltaTime;
            if (_timeAux > .25f && _timeAux < .6f)
            {
                _dummyPunchText.enabled = true;
            }
            if (_timeAux < 0)
            {
                _dummyPunchText.enabled = false;

                _currentWeapon.PlayAnimation("EnemySimpleAttk");

                OnStopAttack();
            }
        }*/
    }

    public override void Exit()
    {
        base.Exit();
    }

}
