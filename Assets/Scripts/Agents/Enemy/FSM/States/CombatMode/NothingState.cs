﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NothingState : IState<Inputs.CombatThink>
{ 


    Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>> _transitions;

    public NothingState()
    {

    }

    public void Enter()
    {

    }

    public void Update()
    {


    }

    public void Exit()
    {

    }

    public Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>> Transitions
    {
        get { return _transitions; }
        set { _transitions = value; }
    }
}
