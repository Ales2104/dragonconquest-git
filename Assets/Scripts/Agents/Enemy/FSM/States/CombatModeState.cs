﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatModeState : IState<Inputs.CommonInputs> , IObserver
{

    public Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> Transitions
    {
        get { return _transitions; }
        set { _transitions = value; }
    }

    Enemy _enemy;
    Human _human;
    Brain _brain;
    float _timeToThink;
    float _timeToThinkAux;
    float _attkRandom;

    Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>> _transitions;
    FSM<Inputs.CombatThink> _fsm;
    AttackState attkState;
    DashState dashState;
    NothingState nothingState;

    public CombatModeState(Enemy e,Weapon currWeapon , Human h, ZoneOfAction zoa, DashSystem dash,ParrySystem parry)
    {
        //attkState = new AttackState(currWeapon);
        
        if (e is Warrior)
            attkState = new WarriorAttk(currWeapon);
        else if (e is Bowman)
            attkState = new BowmanAttk(currWeapon as Bow, h, e as Bowman);


        attkState.OnStopAttack += ChangeToNothingHapp;
        nothingState = new NothingState();
        dashState = new DashState(e,zoa, dash);
        dashState.OnDashHasEnded += DashHasEnded;

        attkState.Transitions = new Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>>();
        attkState.Transitions.Add(Inputs.CombatThink.nothingHappends, nothingState);
        attkState.Transitions.Add(Inputs.CombatThink.didDash, dashState);

        nothingState.Transitions = new Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>>();
        nothingState.Transitions.Add(Inputs.CombatThink.simpleAttk, attkState);
        nothingState.Transitions.Add(Inputs.CombatThink.didDash, dashState);

        dashState.Transitions = new Dictionary<Inputs.CombatThink, IState<Inputs.CombatThink>>();
        dashState.Transitions.Add(Inputs.CombatThink.simpleAttk, attkState);
        dashState.Transitions.Add(Inputs.CombatThink.nothingHappends, nothingState);
        
        _fsm = new FSM<Inputs.CombatThink>(nothingState);

        _enemy = e;
        _human = h;
        

        switch (_enemy.difficult)
        {
            case Enemy.Difficulty.easy:
                _timeToThink = 4;
                break;
            case Enemy.Difficulty.medium:
                _timeToThink = 2;
                break;
            case Enemy.Difficulty.hard:
                _timeToThink = 1f;
                break;
        }

        _brain = new Brain();
    }

    public void Enter()
    {
        Debug.Log("start CombatMode");
        _human.humanFightSys.SuscribeObserver(this);
        _human.humanDefenceSys.SuscribeObserver(this);
        _timeToThinkAux = _timeToThink;
        _attkRandom = UnityEngine.Random.Range(0f, 1f);

        _fsm.ProcessInput(_brain.ExcecuteThink(1));
    }

    public void Update()
    {
        _fsm.Update();

        _enemy.SetXDirFromOtherObject(_human.transform.position);

        /*if (Input.GetKeyDown(KeyCode.F))
        {
            
            _fsm.ProcessInput(Inputs.CombatThink.didDash);
        }*/

        /*if (_fsm.Current == nothingState)
        {
            switch (_enemy.difficult)
            {
                case Enemy.Difficulty.easy:
                    if (_attkRandom <= .8f)
                    {
                        _fsm.ProcessInput(Inputs.CombatThink.simpleAttk);
                    }
                    break;
                case Enemy.Difficulty.medium:
                    if (_attkRandom <= .5f)
                    {
                        _fsm.ProcessInput(Inputs.CombatThink.simpleAttk);
                    }
                    break;
                case Enemy.Difficulty.hard:
                    if (_attkRandom <= .2f)
                    {
                        _fsm.ProcessInput(Inputs.CombatThink.simpleAttk);
                    }
                    break;
            }
        }*/

        Thinking();
    }

    public void Exit()
    {
        _timeToThinkAux = _timeToThink;
        _human.humanFightSys.DesuscribeObserver(this);
        _human.humanDefenceSys.DesuscribeObserver(this);
        Debug.Log("end CombatMode");
    }

    void Thinking()
    {
        if (_fsm.Current != nothingState) return;

        _timeToThinkAux -= Time.deltaTime;
        if (_timeToThinkAux < 0)
        {
            _timeToThinkAux = _timeToThink;
            _fsm.ProcessInput(_brain.ExcecuteThink(1));
        }

    }

    void ChangeToNothingHapp()
    {
        _fsm.ProcessInput(Inputs.CombatThink.nothingHappends);
        
        _attkRandom = UnityEngine.Random.Range(0f, 1f);
    }

    void DashHasEnded()
    {
        _fsm.ProcessInput(Inputs.CombatThink.nothingHappends);
    }

    public void OnNotify(Inputs.ObservableActions act)
    {

        switch (act)
        {
            
            case Inputs.ObservableActions.simpleAttk:
                _brain.AddAction("attacked", Inputs.CombatThink.simpleAttk);
                break;
            case Inputs.ObservableActions.dash:
                _brain.AddAction("dash", Inputs.CombatThink.didDash);
                break;
            case Inputs.ObservableActions.parry:
                _brain.AddAction("parry", Inputs.CombatThink.didParry);
                break;
         
        }
    }
}
