﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM<InputT>
{
    public IState<InputT> Current { get { return currentState; } }
    IState<InputT> currentState;

    public FSM(IState<InputT> initial)
    {
        initial.Enter();
        currentState = initial;
    }

    public void Update()
    {
        currentState.Update();
    }

    public void ProcessInput(InputT input)
    {
        var currentStateTransitions = currentState.Transitions;

        if (currentStateTransitions.ContainsKey(input))
        {
            currentState.Exit();
            currentState = currentStateTransitions[input];
            currentState.Enter();
        }
    }

    
	
}
