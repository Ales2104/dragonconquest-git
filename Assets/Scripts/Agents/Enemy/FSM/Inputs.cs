﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs
{

    public enum CommonInputs {
        NothingHappends,
        GetAttacked,
        PlayerOnRange,
        GetParried,
        ParryOrDashToPlayer,
        PlayerOnSight,
        PlayerTooClose,
        PlayerTooFarAway,
        ArrivedToBase,
        HasFinishSeflHealing
    }

    public enum CombatThink {

        didDash,
        didParry,
        simpleAttk,
        nothingHappends

    }

    //acciones que hizo el observado(player)
    public enum ObservableActions {
        simpleAttk,
        dash,
        parry
    }

}
