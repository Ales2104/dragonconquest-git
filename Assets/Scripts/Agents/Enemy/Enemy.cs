﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Agent
{

    protected bool _humanIsDead;
    protected Human _human;
    protected Brain brain;
    #region FSM

    protected FSM<Inputs.CommonInputs> fsm;
    protected IdleState idleState;
    //protected AttackState attackState;
    protected StunnedState stunnedState;
    protected ReturnBaseState returnBaseState;
    protected SelfHealState selfHealState;
    protected CombatModeState combatModeState;
    #endregion

    protected Vector2 initPosition;//posicion inicial.
    protected ZoneOfAction zoneOfAct;
    protected DashSystem dashSys;
    protected ParrySystem parrySys;
    protected Rigidbody2D rigidBody;

    [Header("FSM params")]

    public Color playerOnRangeDistColor = Color.red;
    [Range(0f, 15f)]
    public float playerOnRangeDistance = 3f;
    [Space(10)]
    [Range(0f, 100f)]
    public float selfHealPercent = 10f;//porcentaje del total de la vida que se recarga por segundo.
    [Tooltip("Time to sum selfHealPercent. 1f = 1 second")]
    [Range(0f, 5f)]
    public float selfHealMultiplier = 1f;
    public bool isSelfHealing;
    [Space(10)]
    public float pauseTimeToGoToBase= 1.5f;//tiempo de pausa antes de regresar a la base

    [Space(20)]
    public Difficulty difficult;


    public enum Difficulty
    {
        easy,
        medium,
        hard
    }

    [Header("Only for tests")]
    public bool canAttack = true;

    protected override void Awake()
    {
        base.Awake();

        brain = new Brain();

        _human = GameObject.FindObjectOfType<Human>();
        if (_human == null)
            Debug.LogError("Human is not in scene or cant be finded");

        _human.OnHumanIsDead += StopEverything;

        if (!canAttack)
        {
            Debug.Log("Dummy named: " + gameObject.name + " , has bool canAttack set as false." );
        }

        dashSys = GetComponentInChildren<DashSystem>();
        rigidBody = GetComponent<Rigidbody2D>(); 
        zoneOfAct = GetComponentInChildren<ZoneOfAction>();
        initPosition = transform.position;

        #region FSM
        idleState = new IdleState();
        //attackState = new AttackState(currentWeapon);
        combatModeState = new CombatModeState(this,currentWeapon,_human, zoneOfAct, dashSys, parrySys);
        stunnedState = new StunnedState(_human.meleeStuntTime, _human , this);
        returnBaseState = new ReturnBaseState(this, rigidBody, initPosition, pauseTimeToGoToBase);
        selfHealState = new SelfHealState(this);

        idleState.Transitions = new Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>>();
        idleState.Transitions.Add(Inputs.CommonInputs.PlayerOnRange, combatModeState);

        combatModeState.Transitions = new Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>>();
        combatModeState.Transitions.Add(Inputs.CommonInputs.GetParried, stunnedState);
        combatModeState.Transitions.Add(Inputs.CommonInputs.PlayerTooFarAway, returnBaseState);//para cuando el pj muere

        stunnedState.Transitions = new Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>>();
        stunnedState.Transitions.Add(Inputs.CommonInputs.PlayerOnRange, combatModeState);
        stunnedState.Transitions.Add(Inputs.CommonInputs.PlayerTooFarAway, returnBaseState);

        returnBaseState.Transitions = new Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>>();
        returnBaseState.Transitions.Add(Inputs.CommonInputs.ArrivedToBase , selfHealState);
        returnBaseState.Transitions.Add(Inputs.CommonInputs.PlayerOnRange, combatModeState);

        selfHealState.Transitions = new Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>>();
        selfHealState.Transitions.Add(Inputs.CommonInputs.HasFinishSeflHealing, idleState);
        selfHealState.Transitions.Add(Inputs.CommonInputs.PlayerOnRange, combatModeState);

        fsm = new FSM<Inputs.CommonInputs>(idleState);
        #endregion


    }

    protected virtual void Update()
    {
        if (!isDead)
        {
            fsm.Update();
            CheckSensors();
        }
        

    }

    void CheckSensors()
    {
        StopStun();
        CheckBoundaries();
        HasArrivedToInitStartPos();
        HasFinishSelfHealing();
    }

    /// <summary>
    /// esta funcion es llamada por parrysystem. Lo que hace es cambiar el current state de la FSM solo si el current es attack
    /// </summary>
    public override void GetParry(float stunTime)
    {   
        if (fsm.Current != combatModeState) return;

        currentWeapon.DisableCollisionZone();
        currentWeapon.StopCurrentAnimation();
        timeInStunMode = stunTime;
        fsm.ProcessInput(Inputs.CommonInputs.GetParried);
    }


    
    ///si esta stuneado, entonces empieza a descontar el tiempo para cambiar a idle.
    void StopStun()
    {
        if (IsStunned())
        {
            timeInStunMode -= Time.deltaTime;
            if (timeInStunMode < 0)
            {
                fsm.ProcessInput(Inputs.CommonInputs.PlayerTooFarAway);
            }
        }
    }


    /// <summary>
    /// Checkea si el jugador está a una cierta distancia como para atacar directamente.
    /// </summary>
    protected virtual void GetPlayerOnRange()
    {
        if (canAttack && !IsStunned() && !_humanIsDead)
        {
            if (Vector2.Distance(transform.position, _human.transform.position) <= playerOnRangeDistance)
            {
                fsm.ProcessInput(Inputs.CommonInputs.PlayerOnRange);
            }
        }
        
    }

    void StopEverything()
    {
        _humanIsDead = true;
        //lo dejo en idle.
        fsm.ProcessInput(Inputs.CommonInputs.PlayerTooFarAway);
    }

    /// <summary>
    /// chequea si está dentro de los limites, si no, pasa al estado Idle(para retornar a su punto de origen)
    /// Siempre debe estar en estado GetInRange para que con este sensor, pase al estado returnBase.
    /// </summary>
    protected virtual void CheckBoundaries()
    {

        if (transform.position.x <= zoneOfAct.leftPos.x || transform.position.x >= zoneOfAct.rightPos.x)
        {
            fsm.ProcessInput(Inputs.CommonInputs.PlayerTooFarAway);
        }
    }

    /// <summary>
    /// si está en estado returnBase entonces se fija si llegó al initStartPos;
    /// </summary>
    protected virtual void HasArrivedToInitStartPos()
    {
        if (fsm.Current != returnBaseState) return;

        var dist = Mathf.Abs(transform.position.x - initPosition.x);

        if (dist <= 0.1f)
        {
            fsm.ProcessInput(Inputs.CommonInputs.ArrivedToBase);
        }
    }

    void HasFinishSelfHealing()
    {
        if (fsm.Current != selfHealState as IState<Inputs.CommonInputs>) return;
        if (isSelfHealing) return;

        Debug.Log("termino el self healing");
        fsm.ProcessInput(Inputs.CommonInputs.HasFinishSeflHealing);
    }

    public bool IsStunned()
    {
        return fsm.Current == stunnedState;
    }
}
