﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : Enemy
{
    GetInRangeState getInRangeState;

    [Header("Warrior")]

    [Space(10)]
    public Color playerOnSightColor = Color.blue;
    [Range(0f, 15f)]
    public float playerOnSightDistance = 5f;
    [Tooltip("Pause before GetInRange start.")]
    [Range(0f,5f)]
    public float timeToGetInRange = 1f;


    float _timeToGetInRangeAux;
    bool _startGetInRange;

    protected override void Awake()
    {
        base.Awake();
        
        getInRangeState = new GetInRangeState(_human,this,rigidBody);
        getInRangeState.Transitions = new Dictionary<Inputs.CommonInputs, IState<Inputs.CommonInputs>>();
        getInRangeState.Transitions.Add(Inputs.CommonInputs.PlayerOnRange, combatModeState);
        getInRangeState.Transitions.Add(Inputs.CommonInputs.PlayerTooFarAway, returnBaseState);

        idleState.Transitions.Add(Inputs.CommonInputs.PlayerOnSight, getInRangeState);
        combatModeState.Transitions.Add(Inputs.CommonInputs.PlayerOnSight, getInRangeState);
        selfHealState.Transitions.Add(Inputs.CommonInputs.PlayerOnSight, getInRangeState);
        returnBaseState.Transitions.Add(Inputs.CommonInputs.PlayerOnSight, getInRangeState);

        _timeToGetInRangeAux = timeToGetInRange;
    }

    protected override void Update()
    {
        base.Update();

        IsPlayerOnSight();
        GetPlayerOnRange();

        if (_startGetInRange)
        {
            //vuelvo a chequear la distancia por si el personaje se fue del rango.
            if (Vector2.Distance(transform.position, _human.transform.position) <= playerOnSightDistance &&
                    Vector2.Distance(transform.position, _human.transform.position) >= playerOnRangeDistance)
            {
                _timeToGetInRangeAux -= Time.deltaTime;
                if (_timeToGetInRangeAux < 0)
                {
                    _startGetInRange = false;
                    _timeToGetInRangeAux = timeToGetInRange;
                    fsm.ProcessInput(Inputs.CommonInputs.PlayerOnSight);
                }
            }
            else
            {
                _startGetInRange = false;
                _timeToGetInRangeAux = timeToGetInRange;
                fsm.ProcessInput(Inputs.CommonInputs.PlayerTooFarAway);
            }

            
        }
    }

    protected override void GetPlayerOnRange()
    {
        base.GetPlayerOnRange();
    }

    void IsPlayerOnSight()
    {
        if (IsStunned() || _startGetInRange)
            return;

        if (transform.position.x >= zoneOfAct.leftPos.x && transform.position.x <= zoneOfAct.rightPos.x)
        {
            if (Vector2.Distance(transform.position, _human.transform.position) <= playerOnSightDistance &&
                Vector2.Distance(transform.position, _human.transform.position) >= playerOnRangeDistance)
            {
                _startGetInRange = true;
            }
            else if (Vector2.Distance(transform.position, _human.transform.position) > playerOnSightDistance)
            {
                //Chequeo que esté mas lejos que el OnSightDistance porque si el 
                //jugador estaba dentro del rango de combate se ejecutaba el 
                //PlayerTooFarAway(cambiaba al estado SelfHealing).
                fsm.ProcessInput(Inputs.CommonInputs.PlayerTooFarAway);

            }
        }  


    }


    void OnDrawGizmos()
    {
        Gizmos.color = playerOnRangeDistColor;
        Gizmos.DrawWireSphere(transform.position, playerOnRangeDistance);

        Gizmos.color = playerOnSightColor;
        Gizmos.DrawWireSphere(transform.position, playerOnSightDistance);
    }
}
