﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashSystem : MonoBehaviour
{
    Agent _agent;
    bool _isDashing;
    Rigidbody2D _rigidBody;
    int _dir;
    float _xVelocitySaved;

    [Header("Can cancel the dash when left stick is moved")]
    public bool canCancelDash;
    [Space(7)]
    [Range(0f, 10f)]
    public float speed = 1f;

    public float timeDashing = 0.5f;

    public Action OnStopDash = delegate { };

    void Start ()
    {
        _agent = GetComponentInParent<Agent>();
        _rigidBody = _agent.GetComponent<Rigidbody2D>();
    }
    
    public void ExecuteDash(float horizontalAxis)
    {
        if (_isDashing) return;
        int normalAxis;

        if (horizontalAxis != 0)
            normalAxis = horizontalAxis < 0 ? -1 : 1;
        else
            normalAxis = _agent.xAxisDirection();
        
        _dir = normalAxis;
        _isDashing = true;
        _agent.imUntouchable = true;
        _xVelocitySaved = _rigidBody.velocity.x;

        _rigidBody.velocity = new Vector2(speed * _agent.runSpeed * normalAxis, _rigidBody.velocity.y);
        StartCoroutine(StartCountdown());
    }


    IEnumerator StartCountdown()
    {
        yield return new WaitForSeconds(timeDashing);
        SetPropsToStopDash();
    }

    /// <summary>
    /// esto se ejecuta siempre que el agente esté dasheando.
    /// Devuelve si puede o no cancelar el dash.
    /// </summary>
    public void CancelDash(float horizAxis)
    {

        if (canCancelDash && horizAxis != _dir)
            SetPropsToStopDash();
    }

    /// <summary>
    /// configura las prop del script para stopear el dash.
    /// </summary>
    void SetPropsToStopDash()
    {
        if (_isDashing)
            OnStopDash();

        _rigidBody.velocity = new Vector2( _xVelocitySaved , _rigidBody.velocity.y );
        _isDashing = false;
        _agent.imUntouchable = false;
        
    }

    
}
