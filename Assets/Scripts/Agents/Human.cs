﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Human : Agent
{

    
    [HideInInspector]
    public Vector3 lastPos;
    [HideInInspector]
    public Vector3 currentPos;

    public event Action OnHumanMovement = delegate { };
    public event Action OnHumanFight = delegate { };
    public event Action OnHumanDefence = delegate { };
    public event Action OnHumanIsDead = delegate { };//cuando el humano muere, le manda un mensaje a todos los suscriptores(enemigos)
    public event Action OnModifyHeath = delegate { };


    [HideInInspector]
    public HumanDefenceSystem humanDefenceSys;
    [HideInInspector]
    public HumanFightSystem humanFightSys;
    [HideInInspector]
    public HumanMoveSystem humanMoveSys;

    List<IPickable> _inventory = new List<IPickable>();

    float initHP;

    protected override void Awake() {
        base.Awake();

        humanDefenceSys = GetComponent<HumanDefenceSystem>();
        humanFightSys = GetComponent<HumanFightSystem>();
        humanMoveSys = GetComponent<HumanMoveSystem>();
        

        lastPos = transform.position;

        initHP = health;
        
    }

    void FixedUpdate()
    {
        OnHumanMovement();
    }

    protected override void IsDeadHandle()
    {
        base.IsDeadHandle();

        humanDefenceSys.Desuscribe();
        humanFightSys.Desuscribe();
        humanMoveSys.Desuscribe();
        OnHumanIsDead();
    }

    void Update ()
    {
        OnHumanFight();
        OnHumanDefence();
        
    }

    public void ManagePickables(IPickable p)
    {
        p.OnPickUp(this);
        OnModifyHeath();
    }

    /// <summary>
    ///
    /// </summary>
    public void SetHP(float val)
    {
        var result = health + val;
        if (result < initHP)
        {
            health = result;
        }
        else
        {
            health = initHP;
        }
    }

    void AddToInventory(IPickable p)
    {
        _inventory.Add(p);
    }

}
