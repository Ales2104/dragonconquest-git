﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObservable 
{
    void SuscribeObserver(IObserver observer);
    void DesuscribeObserver(IObserver observer);
	
}
