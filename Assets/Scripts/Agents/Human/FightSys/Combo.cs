﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[System.Serializable]
public class Combo
{

    public string name;
    [Tooltip("Makes an extra damage when the combo succeed.")]
    [Range(0f,100f)]
    public float dmgOnSuccessPercent = 50f;//porcentaje del ultimo golpe del combo.(base damage del arma).
    public HumanAttackType[] sequence;

    //toma cada el indice del array 'sequence' y les asigna un bool para saber si se cumplió o no la secuiencia.
    Dictionary<int, bool> _sequenceSuccess = new Dictionary<int, bool>();



    /// <summary>
    /// carga el diccionario con los elementos de la secuencia(attk types{key} con falso{value})
    /// </summary>
    public void InitializeCombo()
    {
        for (int i = 0; i < sequence.Length; i++)
        {
            _sequenceSuccess.Add(i, false);
        }
    }

    /// <summary>
    /// Setea el elemento del indice del array "sequence" (que se pasa por parametro) en true(true signifcia que se ejecutó ese comando).
    /// </summary>
    public void SetSuccedSequenceElem(int index)
    {
        
        //recorre los elementos del diccionario en busca de la llave(indice del sequence) y el valor false, y devuelve el primero con valor false
        //var firstElemWithFalseValue = _sequenceSuccess.Where(k => k.Key == index && !k.Value).First();
        //accedo al elemento key-value y le asigno true.
        _sequenceSuccess[index] = true;

    }

    public bool IsComboComplete()
    {
        bool result = false;

        foreach (var item in _sequenceSuccess)
        {
            result = item.Value;
            if (!result) break;
        }
        
        return result;
    }

    /// <summary>
    /// setea todos los valores del diccionario en false(para volver a comenzar)
    /// </summary>
    public void ReturnDictionaryToInitState()
    {
        for (int i = 0; i < _sequenceSuccess.Count; i++)
        {
            _sequenceSuccess[i] = false;
        }
    }


    

}



public enum HumanAttackType { Light, Heavy , AirLight , AirHeavy }
