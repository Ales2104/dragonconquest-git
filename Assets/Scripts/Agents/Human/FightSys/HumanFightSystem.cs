﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class HumanFightSystem : MonoBehaviour, IObservable
{

    #region Privates
    Human _human;
    Weapon _currentWeapon;
    //int _countLightAttack;
    int _comboSequenceIndex = 0;
                            
    IEnumerable<Combo> _currentCombo;//antes de seleccionar el combo "ganador" es un ienumerable de combos. el "ganador" es el que cumple 
                                     //con todos los attk types.

    float _weaponBaseDmgAux;//es el daño original del arma.
    bool _isComboSucced;
    Combo _theWinnerCombo;
    //numero de anim de ataques que tienen que ejecutarse cuando el combo se realizo con exito;
    int _attksToThrow = -1;//-1 es porque no hay ningun combo realizado.

    //Observer
    List<IObserver> _observers = new List<IObserver>();

    #endregion

    #region Publics
    [Header("Make combos(For APB GameDesigners) já")]
    public Combo[] combos;
    [Header("Feedback")]
    public bool isAttacking;
    public bool isComboEnable;
    [Header("Times")]
    //public float timeToLoseCombo = 2f;
    [Tooltip("Time to prohibit attack when combo is unsuccessfull")]
    public float freezeTime = 0.8f;
    #endregion


    // Use this for initialization
    void Awake()
    {
        _human = GetComponent<Human>();
        _currentWeapon = GetComponentInChildren<Weapon>();
        if (_human.animmatorManager != null)
        {
            _human.animmatorManager.OnAttkEnded += AnimAttkEndedHanddler;
        }
        else
        {
            Debug.Log("animmatorManager no isntanciado");
        }
        

        _weaponBaseDmgAux = _currentWeapon.baseDamage;

        if (combos == null || combos.Length == 0) Debug.LogError("Combo is empty or null");

        foreach (var c in combos)
        {
            c.InitializeCombo();
        }

        Suscribe();
    }
    

    void Attack()
    {

        if (_isComboSucced) return;

        var lightBtn = Input.GetButtonDown("LightAttack");
        var heavyBtn = Input.GetButtonDown("HeavyAttack");
        
        if (lightBtn || heavyBtn)
        {
            isAttacking = true;

            //Notifico al enemigo en estado de CombatMode que el jugador pegó.
            NotifyObs(Inputs.ObservableActions.simpleAttk);

            _human.animmatorManager.SetParameter("isAttacking", true);


            if (lightBtn)
            {
                if (_comboSequenceIndex == 0)
                {
                    _currentCombo = combos.Where(c => c.sequence[_comboSequenceIndex] == HumanAttackType.Light)
                                          .ToList();

                    _human.animmatorManager.SetParameter("isLightAttk", true);
                    _human.animmatorManager.ChangeNumOfAttks(3);//numero random ya que siempre se debe ejecutar la primer anim
                    //Desuscribo todos los movimientos que el pj puede hacer.
                    _human.humanMoveSys.DesuscribeAllButNotMove();
                    _human.humanDefenceSys.Desuscribe();

                }
                else if (isComboEnable)
                { //si no es el primer golpe y puede hacer el combo, entonces lo busca en la lista de _currentcombo.
                    _currentCombo = _currentCombo.Where(c => c.sequence.Length >= (_comboSequenceIndex + 1))
                                                 .Where(c => c.sequence[_comboSequenceIndex] == HumanAttackType.Light)
                                                 .ToList();
                }    
                
                foreach (var c in _currentCombo)
                {
                    c.SetSuccedSequenceElem(_comboSequenceIndex);
                }

                _comboSequenceIndex++;
                
            }
            else
            {
                if (_comboSequenceIndex == 0)
                {
                    _currentCombo = combos.Where(c => c.sequence[_comboSequenceIndex] == HumanAttackType.Heavy)
                                          .ToList();

                    _human.animmatorManager.SetParameter("isLightAttk", false);
                    _human.animmatorManager.ChangeNumOfAttks(3);//numero random ya que siempre se debe ejecutar la primer anim

                    //Desuscribo todos los movimientos que el pj puede hacer.
                    _human.humanMoveSys.DesuscribeAllButNotMove();
                    _human.humanDefenceSys.Desuscribe();
                }
                else if (isComboEnable) //si no es el primer golpe y puede hacer el combo, entonces lo busca en la lista de _currentcombo.
                    _currentCombo = _currentCombo.Where(c => c.sequence.Length >= (_comboSequenceIndex + 1))
                                                 .Where(c => c.sequence[_comboSequenceIndex] == HumanAttackType.Heavy)
                                                 .ToList();


                foreach (var c in _currentCombo)
                {
                    c.SetSuccedSequenceElem(_comboSequenceIndex);
                }

                _comboSequenceIndex++;
                
            }



            if (_comboSequenceIndex > 1)//si solo precionó una vez, no se realiza el combo. Como precionó mas de 1 vez, entornces entra al if
            {
                //chequeo si la lista de _currentCombo es 0 es porque precionó mal el boton.
                if (_currentCombo.ToList().Count == 0)
                {

                    //Debug.Log("Combo missed");
                    _comboSequenceIndex = 0;
                    isAttacking = false;
                    _human.humanMoveSys.SuscribeAllButNotMove();
                    _human.humanDefenceSys.Suscribe();

                    StartCoroutine(ExecuteFreezeTime());

                    SetAllCombosSequenceToFalse(combos);
                }
                else if (_currentCombo.ToList().Count == 1)
                {
                    _theWinnerCombo = _currentCombo.ToList().ElementAt(0);

                    if (_theWinnerCombo.IsComboComplete())
                    {
                        _isComboSucced = true;//no permito que se vuelva a atacar hasta que no termine las animaciones del combo.
                        //Debug.Log("Combo Done! combo name is: " + winnerCombo.name);
                        AsignExtraDmgToWeapon(_theWinnerCombo);

                        isAttacking = false;
                        _comboSequenceIndex = 0;
                        SetAllCombosSequenceToFalse(_currentCombo);
                    }
                    else
                    {
                        _isComboSucced = false;
                        _theWinnerCombo = null;
                    }

                }
            }
        }
    }

    void EnableTheCombo()
    {
        isComboEnable = true;
    }
    void DisableTheCombo()
    {
        //Si tiene mas de un combo, chequeo si alguno de estos se completó y tomo el "primer completado"
        //Si _currentCombo == 1 entonces ya se hizo el combo en la funcion Attack();
        //Esto pasa cuando dos o mas combos empiezan con la misma sequencia.
        if (_currentCombo.ToList().Count > 1 )
        {
            _theWinnerCombo = IsAnyComboComplete(_currentCombo);
            if (_theWinnerCombo != null)
            {
                _isComboSucced = true;//no permito que se vuelva a atacar hasta que no termine las animaciones del combo.
                Debug.Log("Combo Done! combo name is: " + _theWinnerCombo.name);//primer completado
                AsignExtraDmgToWeapon(_theWinnerCombo);
            }
            else
            {
                _isComboSucced = false;
                
                //si hay mas de un combo en _currentCombo pero ninguno se completo, entonces suscribo los movimients y la def.
                /*_human.humanMoveSys.SuscribeAllButNotMove();
                _human.humanDefenceSys.Suscribe();
                */
            }
            
        }
        else if (_currentCombo.ToList().Count == 0)
        {
            _isComboSucced = false;
            //si hay mas de un combo en _currentCombo pero ninguno se completo, entonces suscribo los movimients y la def.
            /*_human.humanMoveSys.SuscribeAllButNotMove();
            _human.humanDefenceSys.Suscribe();*/
        }
        
        isComboEnable = false;
        isAttacking = false;
        _comboSequenceIndex = 0;
        SetAllCombosSequenceToFalse(_currentCombo);
        

    }

    void AnimAttkEndedHanddler()
    {
        if (!_isComboSucced && _theWinnerCombo == null) 
        {
            _human.humanMoveSys.SuscribeAllButNotMove();
            _human.humanDefenceSys.Suscribe();
            _human.animmatorManager.FinishAttkStateMachine();
            _human.animmatorManager.SetParameter("isAttacking", false);
            return;//si es un simple golpe, que retorne.
        }

        if (_attksToThrow == -1)
            _attksToThrow = _theWinnerCombo.sequence.Length;

        _attksToThrow--;//como ya realizó la primer anim de ataque, descuento uno.

        if (_attksToThrow > 0)
        {
            _human.animmatorManager.ChangeNumOfAttks(_theWinnerCombo.sequence.Length);
        }
        else
        {
            _human.animmatorManager.FinishAttkStateMachine();
            //al terminar todas las anim del combo, vuelvo a suscribir toda la def y el movimiento.
            _human.humanMoveSys.SuscribeAllButNotMove();
            _human.humanDefenceSys.Suscribe();
            _isComboSucced = false;
            _attksToThrow = -1;
            _theWinnerCombo = null;
        }

    }

    /// <summary>
    /// Corrutina utilizada para desregistrarce del evento OnHumanFight durante el freezetime.
    /// </summary>
    IEnumerator ExecuteFreezeTime()
    {
        _human.OnHumanFight -= Attack;
        isAttacking = false;
        Debug.Log("Freeze time Start");
        _human.animmatorManager.FreezeCurrentAnimation(true);

        yield return new WaitForSeconds(freezeTime);

        _human.animmatorManager.FreezeCurrentAnimation(false);
        _human.animmatorManager.FinishAttkStateMachine();
        _human.OnHumanFight += Attack;
        Debug.Log("Freeze time Ends");
    }

    #region Comented Functions

    /// <summary>
    /// Tiempo en que se puede ejecutar el proximo movimiento para seguir con el combo, si se pierde éste tiempo,
    /// se pierde el combo.
    /// </summary>
    /*IEnumerator ComboLifeTime()
    {
        yield return new WaitForSeconds(timeToLoseCombo);
        Debug.Log("Time to continue combo is empty.");
        isAttacking = false;
        _comboSequenceIndex = 0;
    }*/

    /// <summary>
    /// Esta funcion se llama solamente cuando el currentCombo tiene un solo elemento.
    /// </summary>
    //bool CheckSequesceComboIsFinished(Combo pCombo)
    //{
    //    return pCombo.sequence.Length == _comboSequenceIndex;
    //}
    #endregion

    public void Suscribe()
    {
        _human.OnHumanFight += Attack;
        _human.animmatorManager.OnComboEnabled += EnableTheCombo;
        _human.animmatorManager.OnComboDisabled += DisableTheCombo;
        
    }
    public void Desuscribe()
    {
        _human.OnHumanFight -= Attack;
        _human.animmatorManager.OnComboEnabled -= EnableTheCombo;
        _human.animmatorManager.OnComboDisabled -= DisableTheCombo;
        
    }
    
    Combo IsAnyComboComplete(IEnumerable<Combo> pCombos)
    {
        
        foreach (var c in pCombos)
        {
            if (c.IsComboComplete())
                return c;
        }

        return null;
    }

    void SetAllCombosSequenceToFalse(IEnumerable<Combo> pCombos)
    {
        foreach (var item in pCombos)
        {
            item.ReturnDictionaryToInitState();
        }
    }

    /// <summary>
    /// asigna daño extra al arma solo si se cumple con el combo(se lo agrega al ultimo golpe).
    /// </summary>
    void AsignExtraDmgToWeapon(Combo winnerCombo)
    {
        

        _currentWeapon.SetExtraDmg(winnerCombo.dmgOnSuccessPercent);

        _currentWeapon.OnAttackSucced += AttackHasSucced;
    }

    /// <summary>
    /// se ejecuta cuando el pj le pega el enemigo.
    /// Devuelve el base damage del arma al estado comun.(pasa cuando un combo se genera con exito y se le agrega un daño extra al arma).
    /// </summary>
    void AttackHasSucced()
    {
        if (_currentWeapon.baseDamage != _weaponBaseDmgAux)
            _currentWeapon.baseDamage = _weaponBaseDmgAux;

        _currentWeapon.OnAttackSucced -= AttackHasSucced;
    }

    public void SuscribeObserver(IObserver observer)
    {
        if (!_observers.Contains(observer))
        {
            _observers.Add(observer);
        }
    }

    public void DesuscribeObserver(IObserver observer)
    {
        if (_observers.Contains(observer))
        {
            _observers.Remove(observer);
        }
    }
    

    void NotifyObs(Inputs.ObservableActions act)
    {
        foreach (var obs in _observers)
        {
            obs.OnNotify(act);
        }
    }
}
