﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanDefenceSystem : MonoBehaviour , IObservable
{

    Human _human;
    ParrySystem _parrySystem;
    DashSystem _dashSystem;

    List<IObserver> _observers = new List<IObserver>();

    void Awake()
    {
        _human = GetComponent<Human>();

        _parrySystem = GetComponentInChildren<ParrySystem>();
        if (_parrySystem == null)
            Debug.LogError("parry is not in child object");

        _dashSystem = GetComponentInChildren<DashSystem>();
        if (_dashSystem == null)
            Debug.LogError("dash is not in child object");

        Suscribe();
    }


    void Update() {

    }

    public void Suscribe()
    {
        _human.OnHumanDefence += DetectParry;
        _human.OnHumanDefence += DetectDash;
        _dashSystem.OnStopDash += DetectEndOfDash;
        _parrySystem.OnParryStop += ActivateMoveWhenParryIsFinished;
    }

    public void Desuscribe()
    {
        _human.OnHumanDefence -= DetectParry;
        _human.OnHumanDefence -= DetectDash;
        _dashSystem.OnStopDash -= DetectEndOfDash;
        _parrySystem.OnParryStop -= ActivateMoveWhenParryIsFinished;
    }

    void DetectDash()
    {
        
        var pressedBtn = Input.GetButtonDown("Dash");
        
        if (pressedBtn && !_human.imInAir)
        {
            var horizontalAxis = Input.GetAxis("Horizontal");

            _human.humanMoveSys.Desuscribe();
            _human.humanFightSys.Desuscribe();

            _human.animmatorManager.SetParameter("isDashing", true);

            _dashSystem.ExecuteDash(horizontalAxis);
            
            _human.OnHumanDefence += CancelDash;

            NotifyObs(Inputs.ObservableActions.dash);
        }
    }

    /// <summary>
    /// Solo lo uso para saber cuando terminó el dash, y asi activar el movement del humano y su ataque.
    /// </summary>
    void DetectEndOfDash()
    {
        _human.humanMoveSys.Suscribe();
        _human.humanFightSys.Suscribe();
    }

    void CancelDash()
    {
        var horizontalAxis = Input.GetAxis("Horizontal");

        if (horizontalAxis == 1 || horizontalAxis == -1)
        {
            _dashSystem.CancelDash(horizontalAxis);
            
            _human.OnHumanDefence -= CancelDash;
        }

        
    }

    void DetectParry()
    {
        
        var horizontalAxis = Input.GetAxis("Parry Horizontal");
        var verticalAxis = Input.GetAxis("Parry Vertical");

        if (horizontalAxis == 1 || horizontalAxis == -1)
        {
            if (!_parrySystem.isInParryMode )
            {
                _human.humanMoveSys.DesuscribeAllButNotMove();
                _parrySystem.StartParry(_human.xAxisDirection(), horizontalAxis);
            }
            
        }
    }

    void ActivateMoveWhenParryIsFinished()
    {
        _human.humanMoveSys.SuscribeAllButNotMove();
    }

    public void DesuscribeParry()
    {
        _human.OnHumanDefence -= DetectParry;
    }

    public void SuscribeParry()
    {
        _human.OnHumanDefence += DetectParry;
    }

    public void SuscribeObserver(IObserver observer)
    {
        if (!_observers.Contains(observer))
        {
            _observers.Add(observer);
        }
    }

    public void DesuscribeObserver(IObserver observer)
    {
        if (_observers.Contains(observer))
        {
            _observers.Remove(observer);
        }
    }


    void NotifyObs(Inputs.ObservableActions act)
    {
        foreach (var obs in _observers)
        {
            obs.OnNotify(act);
        }
    }
}

