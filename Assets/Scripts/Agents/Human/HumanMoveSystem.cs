﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HumanMoveSystem: MonoBehaviour
{
    //privates
    Human _human;
    Action Movement;
    Action GORotation;
    Action JumpSystem;
    float _currentVelocity = 0f;
    Rigidbody2D _rigidBody;
    float _distanceToGround;
    int _floorLayer;
    CapsuleCollider2D _capsuleCol;
    bool _imBeingHurt;
    float _timeToJumpAfterLeavePlatform = .08f;//pequeño delay para permitirle saltar al jugador una vez que dejo de pisar la platform.
    //

    //publics
    public SpriteRenderer _playerSpriteRender;//es publica solo por los gizmos.
    [Range(0f, 10f)]
    public float decelerationRate = 2f;
    [Range(0f, 15f)]
    [Tooltip("0 is nothing")]
    public float accelerationOnFall = 5f;
    [Header("Estando quieto")]
    public Vector2 jumpForce = new Vector2(0,4);
    [Header("En movimiento")]
    public Vector2 jumpForceOnMove = new Vector2(0, 5);


    void Start ()
    {
        _human = GetComponent<Human>();
        _human.currentArmor.OnHurtAgent += StopMovementOnDamageHanddler;
        _human.animmatorManager.OnStopTakeDmg += StartMovementAfterDmgHanddler;
        _rigidBody = GetComponent<Rigidbody2D>();
        _capsuleCol = GetComponent<CapsuleCollider2D>();
        _playerSpriteRender = GetComponentInChildren<SpriteRenderer>();
        _distanceToGround = _playerSpriteRender.bounds.extents.y;

        //guardo funcion en delegate asi suscribo y desuscribo.    
        Movement = HumanMovement;
        GORotation = RotateGO;
        Suscribe();

        _floorLayer = LayerMask.NameToLayer("Floor");
        
    }

    void FixedUpdate()
    {
        if (IsFallen())
            AccelerateOnFall();
        
    }

    #region Horizontal Movement
    void HumanMovement()
    {

        var horizontalAxis = Input.GetAxis("Horizontal");


        if (horizontalAxis == 0)
            _currentVelocity = Mathf.Lerp(_currentVelocity , 0f , decelerationRate * Time.deltaTime);
        else
            _currentVelocity = horizontalAxis * _human.walkSpeed;


        if (Input.GetButton("Run"))
        {
            _currentVelocity = horizontalAxis * _human.runSpeed;

            if (!_human.imInAir)
                _human.animmatorManager.SetRun(true);
            else
                _human.animmatorManager.SetRun(false);

        }
        else
        {
            _human.animmatorManager.SetRun(false);
        }



        if (_human.humanFightSys.isAttacking)
        {
            _currentVelocity = horizontalAxis * _human.moveSpeedOnAttack;

        }
        
        //esto funca
        //transform.position += new Vector3(currentVelocity, 0, 0);
        //pero va a hacer con rigidbody el movimiento
        _rigidBody.velocity = new Vector2(_currentVelocity , _rigidBody.velocity.y);
        if (!_human.imInAir)
        {
            _human.animmatorManager.SetParameter("Speed", Mathf.Abs(horizontalAxis));
        }
        else
        {
            _human.animmatorManager.SetParameter("Speed", 0);
        }
        
        

    }

    /// <summary>
    /// Rota el objecto cuando cambia de direccion.
    /// </summary>
    void RotateGO()
    {
        var horizontalAxis = Input.GetAxis("Horizontal");

        if (horizontalAxis == 0) return;

        transform.right = Vector3.Normalize(new Vector3(horizontalAxis , 0 , 0));
        
    }
    #endregion
    
    void AccelerateOnFall()
    {
        _rigidBody.AddForce(- transform.up * accelerationOnFall, ForceMode2D.Force);
    }

    void Jumping()
    {
        var pressedBtn = Input.GetButtonDown("Jump");
        
        if (pressedBtn && IsGrounded() )
        {
            var horizAxis = Input.GetAxis("Horizontal");
            
            var platformVelY = _human.iAmOnPlatform ? _human.stayingPlatform.currentVelocityY : 0;
            platformVelY = platformVelY > 0 ? platformVelY : 0;

            if (IsMovingHorizontal())
                _rigidBody.velocity = new Vector2(horizAxis, jumpForceOnMove.y + platformVelY);
            else
                _rigidBody.velocity = new Vector2(horizAxis, jumpForce.y + platformVelY);

            _human.animmatorManager.SetParameter("isJumping",true);
            _human.humanDefenceSys.DesuscribeParry();
        }

        if (IsGrounded())
        {
            if (_human.imInAir)
                _human.humanDefenceSys.SuscribeParry();

            _human.imInAir = false;
            _human.animmatorManager.SetParameter("isJumping", false);
        }
        else
        {
            _human.imInAir = true;
        }
    }
    
    /// <summary>
    /// Si está tocando el piso...
    /// </summary>
    bool IsGrounded()
    {

        RaycastHit2D middleHit = Physics2D.Raycast(_playerSpriteRender.bounds.center, -transform.up , (_distanceToGround + .1f), 1 << _floorLayer);
        var isMiddleCollision = middleHit.collider != null && !middleHit.collider.isTrigger;

        /*En vez de tirar 3 rayos, voy a tirar uno en el medio, y cuento el tiempo desde que toco la plat 
         por ultima vez.*/
        if (!isMiddleCollision)
        {
            _timeToJumpAfterLeavePlatform -= Time.fixedDeltaTime;

            return _timeToJumpAfterLeavePlatform >= 0;
        }
        else
        {
            _timeToJumpAfterLeavePlatform = .08f;
            return isMiddleCollision;
        }

        /*Lo comentado funca.
         * var correctMax = new Vector3(_playerSpriteRender.bounds.max.x, _playerSpriteRender.bounds.center.y);
        RaycastHit2D rightHit = Physics2D.Raycast(correctMax, -transform.up, (_distanceToGround + .1f), 1 << _floorLayer);

        var correctMin = new Vector3(_playerSpriteRender.bounds.min.x, _playerSpriteRender.bounds.center.y);
        RaycastHit2D leftHit = Physics2D.Raycast(correctMin, -transform.up, (_distanceToGround + .1f), 1 << _floorLayer);
        */

        /* var isRightCollision = rightHit.collider != null && !rightHit.collider.isTrigger;
         var isLeftCollision = leftHit.collider != null && !leftHit.collider.isTrigger;
         return isMiddleCollision || isRightCollision || isLeftCollision;*/

    }

    void PickUpThings()
    {
        if (Input.GetButtonDown("Action"))
        {
            var y = transform.position.y - (transform.position.y / 2);
            var final = new Vector2(transform.position.x, y);
            var pickedOne = Physics2D.OverlapCircleAll(final, 1f)
                                        .Where(p => p.GetComponent<IPickable>() != null);
            if (pickedOne.Any())
            {
                
                _human.ManagePickables(pickedOne.First().GetComponent<IPickable>());
            }

        }
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.blue;
        //Gizmos.DrawLine(_playerSpriteRender.bounds.center, new Vector3(_playerSpriteRender.bounds.center.x, _playerSpriteRender.bounds.center.y - (_distanceToGround+ .1f)));

        //Gizmos.color = Color.red;
        //var maxY =  _playerSpriteRender.bounds.center.y;
        //Gizmos.DrawLine(new Vector3(_playerSpriteRender.bounds.max.x, _playerSpriteRender.bounds.center.y), new Vector3(_playerSpriteRender.bounds.max.x, maxY - (_distanceToGround + .1f)));

        //Gizmos.color = Color.yellow;
        //var minY =  _playerSpriteRender.bounds.center.y;
        //Gizmos.DrawLine(new Vector3(_playerSpriteRender.bounds.min.x, _playerSpriteRender.bounds.center.y), new Vector3(_playerSpriteRender.bounds.min.x, minY - (_distanceToGround + .1f)));

        //var y = transform.position.y - (transform.position.y / 2);
        //var final = new Vector2(transform.position.x, y);
        //Gizmos.DrawWireSphere(final, 1f);
    }

    public void Suscribe()
    {

        _human.OnHumanMovement += Movement;
        _human.OnHumanMovement += GORotation;
        _human.OnHumanMovement += Jumping;
        _human.OnHumanMovement += PickUpThings;
    }

    /// <summary>
    /// desuscribe todas las funciones "attachadas" a human.OnHumanMovement
    /// </summary>
    public void Desuscribe()
    {
        
        _human.OnHumanMovement -= Movement;
        _human.OnHumanMovement -= GORotation;
        _human.OnHumanMovement -= Jumping;
        _human.OnHumanMovement -= PickUpThings;
    }

    /// <summary>
    /// no desuscribe el movimiento.(para el attaque).
    /// </summary>
    public void DesuscribeAllButNotMove()
    {
        _human.OnHumanMovement -= GORotation;
        _human.OnHumanMovement -= Jumping;
        _human.OnHumanMovement -= PickUpThings;
    }
    /// <summary>
    /// no suscribe el movimiento.(para el attaque).
    /// </summary>
    public void SuscribeAllButNotMove()
    {
        _human.OnHumanMovement += GORotation;
        _human.OnHumanMovement += Jumping;
        _human.OnHumanMovement += PickUpThings;
    }

    /// <summary>
    /// Se llama a esta funcion cuando le hacen daño al pj.
    /// </summary>
    void StopMovementOnDamageHanddler()
    {
        //Debug.Log("Desuscribe movement");
        _rigidBody.velocity = new Vector2(0, _rigidBody.velocity.y);
        Desuscribe();
    }
    /// <summary>
    /// Se llama a esta funcion cuando termina la animacion de TakeDmg.
    /// </summary>
    void StartMovementAfterDmgHanddler()
    {
        //Debug.Log("Suscribe movement");
        Suscribe();
    }


    #region Helpers
    bool IsFallen()//si está cayendo...
    {
        return _rigidBody.velocity.y < 0;
    }

    bool IsMovingHorizontal()
    {
        return Input.GetAxis("Horizontal") != 0;
    }

    #endregion

}
