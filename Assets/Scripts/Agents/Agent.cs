﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour
{

    public float health;
    
    //public float strength = 20f;//variable al pedo(ya que el daño lo hace la propia arma.

    [Header("Movement")]
    [Range(0, 20)]
    public float walkSpeed = 4;

    [Range(0, 20)]
    public float runSpeed = 7;

    [Range(0, 5)]
    public float moveSpeedOnAttack= 1.8f;

    public Weapon currentWeapon;
    public Armor currentArmor;

    public bool isDead;

    [Header("Parry")]
    public bool canParry;
    [Tooltip("El tiempo que dura la ACCION del parry.")]
    [Range(0f, 3f)]
    public float parryTime = 0.3f;

    [Tooltip("Cuanto dura el stunt en el que recibe el parry(warrior o jugador)")]
    [Range(0f, 10f)]
    public float meleeStuntTime = 1f;

    protected float timeInStunMode;
    [Space(10)]
    public bool imInAir;//cuando está en el aire, por un salto o porque calló.

    public bool imUntouchable;//cuando se está dasheando, no podes recibir daño.

    [HideInInspector]//la plataforma en la que el personaje está parado
    public MovingPlatform stayingPlatform;
    [HideInInspector]
    public bool iAmOnPlatform;

    [HideInInspector]
    public AnimatorManager animmatorManager;

    // Use this for initialization
    protected virtual void Awake ()
    {
        currentArmor = GetComponentInChildren<Armor>();
        currentWeapon = GetComponentInChildren<Weapon>();
        currentArmor.OnAgentIsDead += IsDeadHandle;
        animmatorManager = GetComponentInChildren<AnimatorManager>();
    }

    public virtual void GetParry(float stunTime)
    {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    protected virtual void IsDeadHandle()
    {
        isDead = true;
    }

    public int xAxisDirection()
    {
        return (int)transform.right.x;
    }

    public void SetXDirFromOtherObject(Vector3 goPosition)
    {
        var dir = (goPosition - transform.position).normalized.x;
        transform.right = -Vector3.Normalize(new Vector3(dir, 0, 0));
    }
}
