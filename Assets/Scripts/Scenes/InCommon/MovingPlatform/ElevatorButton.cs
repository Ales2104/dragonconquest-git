﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorButton : MonoBehaviour
{

    [Header("Button is in start position of elevator?")]
    public bool isInStartPos = true;

    public Action<bool> OnCallElevator = delegate { };
    int layerPlayer;
    Color _startColor;
    bool _isPressed;

	// Use this for initialization
	void Start () {
		layerPlayer = LayerMask.NameToLayer("Player");
        _startColor = GetComponentInChildren<Renderer>().material.color;
    }
    
    private void OnTriggerStay2D(Collider2D collision)
    {
        
        if (collision.gameObject.layer == layerPlayer && Input.GetButtonDown("Action") && !_isPressed)
        {
            OnCallElevator(isInStartPos);
            _isPressed = true;

            gameObject.GetComponentInChildren<Renderer>().material.color = new Color(0,0,0);
            StartCoroutine(ReturnToOriginalColor());
        }
    }

    IEnumerator ReturnToOriginalColor()
    {
        yield return new WaitForSeconds(0.3f);
        gameObject.GetComponentInChildren<Renderer>().material.color = _startColor;
        _isPressed = false;
    }
}
