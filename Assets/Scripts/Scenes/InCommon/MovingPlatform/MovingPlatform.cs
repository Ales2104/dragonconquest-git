﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

    [Range(0f, 15f)]
    public float moveSpeed = 3f;

    [Range(0f, 5f)]
    public float timeToMove = 0.7f;
    float _timeToMoveAux;

    public bool isAnElevator;

    Vector2 _startPosition;
    Vector2 _endPosOnStart;//la posicion del endpos al comienzo del juego;
    public Transform endPosition;

    [HideInInspector]
    public float currentVelocityY;
    
    Rigidbody2D _rigidBody;

    bool _btnWasPressed;
    bool _isInStartPosition = true;//verifico si el elevador esta en la primer posicion de dnd arrancó;
    float _previousPosY;
    bool _startCountDown;//para cuando el personaje entra al col
    int playerLayer;

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        GetComponentInChildren<AgentDetection>().OnPlayerEnter += DetectAgentInsideMe;

        
        _startPosition = transform.position;
        _endPosOnStart = endPosition.position;

        _timeToMoveAux = timeToMove;
        _previousPosY = transform.position.y;

        if (endPosition == null)
            Debug.LogError("EndPosition component is MISSED. in gameobject named => " + transform.parent.name);

        if (!isAnElevator)
            _startCountDown = true;
        else
            transform.parent.GetComponentInChildren<ElevatorButton>().OnCallElevator += CallElevatorHanddle;

        playerLayer = LayerMask.NameToLayer("Player");
    }


    void FixedUpdate()
    {
        if (_startCountDown)
        {
            if (isAnElevator)
            {
                _timeToMoveAux -= Time.deltaTime;
                if (_timeToMoveAux < 0)
                {
                    transform.position = Vector2.MoveTowards(transform.position, endPosition.position, Time.fixedDeltaTime * moveSpeed);
                    
                    currentVelocityY = (transform.position.y - _previousPosY) / Time.fixedDeltaTime;
                }
            }
            else
            {
                transform.position = Vector2.MoveTowards(transform.position, endPosition.position, Time.fixedDeltaTime * moveSpeed);
                currentVelocityY = (transform.position.y - _previousPosY) / Time.fixedDeltaTime;
            }

        }

        ProcessElevator();

        _previousPosY = transform.position.y;
    }


    void ProcessElevator()
    {
        var dist = Vector2.Distance(transform.position, endPosition.position);

        if (dist < 0.1f)
        {
            _timeToMoveAux = timeToMove;
            if (isAnElevator )
            {
               
                _startCountDown = false;
            }
            
            var aux = _startPosition;
            _startPosition = endPosition.position;
            endPosition.position = aux;
            _isInStartPosition = !_isInStartPosition;
            currentVelocityY = 0;
        }
    }

    void DetectAgentInsideMe()
    {
        if (isAnElevator && !_startCountDown)
            _startCountDown = true;
    }

    void CallElevatorHanddle(bool btnPressIsInStartPos)
    {
        if (btnPressIsInStartPos != _isInStartPosition)
        {
            _startCountDown = true;
            _timeToMoveAux = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == playerLayer)
        {
            collision.transform.SetParent(transform);
            
            var agent = collision.gameObject.GetComponent<Agent>();
            agent.iAmOnPlatform = true;

            if (agent.stayingPlatform == null || !agent.stayingPlatform.Equals(this))
            {
                agent.stayingPlatform = this;
            }

        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == playerLayer)
        {
            collision.transform.SetParent(null);
            var agent = collision.gameObject.GetComponent<Agent>();
            agent.iAmOnPlatform = false;
            
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, endPosition.position);
    }

}
