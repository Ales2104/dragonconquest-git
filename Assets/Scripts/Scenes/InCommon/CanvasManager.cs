﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour 
{

    public Human human;
    float _InitHealth;
    public Image daño1;
    public Image daño2;
    public Image daño3;


	// Use this for initialization
	void Start () 
    {
        human.currentArmor.OnHurtAgent += ChangeMainCharHealthHanddler;
        human.OnModifyHeath += ChangeMainCharHealthHanddler;
        _InitHealth = human.health;
	}
	
    /// <summary>
    /// Ademas de cambiar la GUI por cada vez que el daño cambia, tmb cambia cuando el jugador toma una pota.
    /// </summary>
    void ChangeMainCharHealthHanddler()
    {

        var percent = (human.health * 100) / _InitHealth;

        if (percent >= 100)
        {
            daño3.enabled = false;
            daño2.enabled = false;
            daño1.enabled = false;
            return;
        }

        if (percent > 66)
        {
            daño3.enabled = false;
            daño2.enabled = false;
            daño1.enabled = true;
            if(percent > 85)
                daño1.color = new Color(daño1.color.r, daño1.color.g, daño1.color.b, 125);
            else
                daño1.color = new Color(daño1.color.r, daño1.color.g, daño1.color.b, 255);
            
        }
        else if (percent > 33)
        {
            daño2.enabled = true;
            daño1.enabled = false;
            daño3.enabled = false;

            if (percent > 50)
                daño2.color = new Color(daño2.color.r, daño2.color.g, daño2.color.b, 125);
            else
                daño2.color = new Color(daño2.color.r, daño2.color.g, daño2.color.b, 255);
        }
        else
        {
            daño2.enabled = false;
            daño1.enabled = false;
            daño3.enabled = true;

            if (percent > 15)
                daño2.color = new Color(daño2.color.r, daño2.color.g, daño2.color.b, 125);
            else
                daño2.color = new Color(daño2.color.r, daño2.color.g, daño2.color.b, 255);
        }
        
    }
}

